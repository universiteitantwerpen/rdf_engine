package ua.dbproject.db;

import static org.junit.Assert.*;

import java.sql.SQLException;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;


public class DbControllerTest {
	
	private static DbController testDbController;
	
	@Before
	public void setUp() throws Exception {
		testDbController = new DbController();
	}
		
	@Test
	public void testConnect() throws SQLException, ClassNotFoundException {
		assertTrue(testDbController.connect());
	}

	@Test
	public void testInsert() throws SQLException, ClassNotFoundException {
		testDbController.connect();
		assertTrue(testDbController.executeUpdate("INSERT INTO tripleSPO (subject, predicate, object) values (1, 2, 3);"));
		testDbController.executeUpdate("DELETE FROM tripleSPO WHERE subject = 1;");
	}
	
	@Test
	public void testDelete() throws SQLException, ClassNotFoundException {
		testDbController.connect();
		testDbController.executeUpdate("INSERT INTO tripleSPO (subject, predicate, object) values (4, 5, 6);");
		assertTrue(testDbController.executeUpdate("DELETE FROM tripleSPO WHERE subject = 4;"));
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {		
		testDbController.close();
	}

}
