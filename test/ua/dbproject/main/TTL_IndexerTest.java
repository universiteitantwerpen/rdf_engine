package ua.dbproject.main;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.Test;

public class TTL_IndexerTest {

	@Test
	public void testMain() throws ClassNotFoundException, IOException, SQLException {
		TTL_Indexer.main(new String[] { "yagoFacts.ttl" });
	}
	
	@Test
	public void test3kbNt() throws ClassNotFoundException, IOException, SQLException {
		TTL_Indexer.main(new String[] { "fb1line.nt" });// 0secs with MapDbDict, 0secs with DbDict
	}
	
	@Test
	public void testNt50mbSplit() throws ClassNotFoundException, IOException, SQLException {
		TTL_Indexer.main(new String[] { "50mb.nt" }); // 120secs with MapDbDict, 16secs with DbDict, 20
	}
	
	@Test
	public void testNt200mbSplit() throws ClassNotFoundException, IOException, SQLException {
		TTL_Indexer.main(new String[] { "200mb.nt" }); // 415secs with MapDbdict, 66secs with DbDict, 91
	}
	
	@Test
	public void testNt600mbSplit() throws ClassNotFoundException, IOException, SQLException {
		TTL_Indexer.main(new String[] { "600mb.nt" }); // 463secs with MapDbDict, 199secs with DbDict
	}
	
	@Test
	public void testNt1200mbSplit() throws ClassNotFoundException, IOException, SQLException {
		TTL_Indexer.main(new String[] { "1200mb.nt" }); //2700secs with MapDbDict, 418secs with DbDict
	}
	
	@Test
	public void testNtSample() throws ClassNotFoundException, IOException, SQLException {
		TTL_Indexer.main(new String[] { "fb_sample.nt" });// N/A with MapDbDict, secs with DbDict
	}
	
	@Test
	public void testNt() throws ClassNotFoundException, IOException, SQLException {
		TTL_Indexer.main(new String[] { "fb.nt" });// N/A with MapDbDict, secs with DbDict
	}
	
	@Test
	public void testIndexRes() throws ClassNotFoundException, IOException, SQLException {
		TTL_Indexer.main(new String[] { "/Users/ktheodorakos/Downloads/results/q11.txt" });// N/A with MapDbDict, secs with DbDict
	}
	
	@Test
	public void testIndexResAll() throws ClassNotFoundException, IOException, SQLException {
		ResultsIndexer.main(new String[] { });// N/A with MapDbDict, secs with DbDict
	}
}