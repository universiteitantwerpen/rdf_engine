package ua.dbproject.main;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Test;

import ua.dbproject.indexer.Triple;
import ua.dbproject.main.SPARQL_Processor;
import ua.dbproject.parsers.RdfItem;
import ua.dbproject.parsers.SPARQL_Parser;
import ua.dbproject.parsers.SparqlQuery;

public class SPARQL_ProcessorFbNtSampleTest {
	@Test
	public void testGetRdfItemsAll() throws ClassNotFoundException, IOException, SQLException {
		String[] allQueries = new String[] {
				"SELECT ?s, ?o1, ?o2 WHERE {?s asdt ?o1 .?o1 234fdf ?o2 .}",
				"SELECT ?s, ?o WHERE { ?s <http://rdf.freebase.com/ns/common.topic.webpage> ?o .}",
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/location.location.geolocation> ?o .}",
				"SELECT ?s, ?o1, ?o2 WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o2 .}",
				"SELECT ?s, ?o1, ?o2 WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .}",
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film.genre> ?o0 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?s <http://www.w3.org/2002/07/owl#sameAs> ?o2 .}",
				"SELECT DISTINCT ?o1, ?o2 WHERE {?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o2 .?o2 <http://rdf.freebase.com/ns/film.performance.film> ?o3 .}",
				"SELECT ?s, ?o1, ?o2, ?o3, ?o4 WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o3 .?o3 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .}",
				"SELECT DISTINCT ?s, ?o5 WHERE { ?s <http://rdf.freebase.com/ns/common.topic.article> ?o1 .?s <http://creativecommons.org/ns#attributionName> ?o2 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o3 .?o3 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .?o4 <http://rdf.freebase.com/ns/film.film.starring> ?o5 .}",
				"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o3 .?o3 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .?o4 <http://rdf.freebase.com/ns/film.film.starring> ?o5 .?o5 <http://rdf.freebase.com/ns/film.performance.film> ?o6 .?o6 <http://rdf.freebase.com/ns/film.film.starring> ?o7 .}",
				"SELECT DISTINCT ?s, ?o8 WHERE {?s <http://rdf.freebase.com/ns/common.topic.image> ?o1 .?s <http://creativecommons.org/ns#attributionURL> ?o2 .?s <http://rdf.freebase.com/ns/type.object.name> ?o3 .?s <http://rdf.freebase.com/ns/film.film.directed_by> ?o4 .?s <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o5 .?s <http://creativecommons.org/ns#attributionName> ?o6 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o7 .?o7 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .}"
				};
		
		SPARQL_Processor.main(allQueries);
	}
	
	@Test
	public void testGetRdfItemsSome() throws ClassNotFoundException, IOException, SQLException {
		String[] allQueries = new String[] {
				"SELECT ?s, ?o WHERE { ?s <http://rdf.freebase.com/ns/common.topic.webpage> ?o .}",
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/location.location.geolocation> ?o .}",
//				"SELECT ?s, ?o1, ?o2 WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o2 .}",
//				"SELECT ?s, ?o1, ?o2 WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .}",
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film.genre> ?o0 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?s <http://www.w3.org/2002/07/owl#sameAs> ?o2 .}",
//				"SELECT DISTINCT ?o1, ?o2 WHERE {?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o2 .?o2 <http://rdf.freebase.com/ns/film.performance.film> ?o3 .}",
//				"SELECT ?s, ?o1, ?o2, ?o3, ?o4 WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o3 .?o3 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .}",
//				"SELECT DISTINCT ?s, ?o5 WHERE { ?s <http://rdf.freebase.com/ns/common.topic.article> ?o1 .?s <http://creativecommons.org/ns#attributionName> ?o2 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o3 .?o3 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .?o4 <http://rdf.freebase.com/ns/film.film.starring> ?o5 .}",
//				"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o3 .?o3 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .?o4 <http://rdf.freebase.com/ns/film.film.starring> ?o5 .?o5 <http://rdf.freebase.com/ns/film.performance.film> ?o6 .?o6 <http://rdf.freebase.com/ns/film.film.starring> ?o7 .}",
//				"SELECT DISTINCT ?s, ?o8 WHERE {?s <http://rdf.freebase.com/ns/common.topic.image> ?o1 .?s <http://creativecommons.org/ns#attributionURL> ?o2 .?s <http://rdf.freebase.com/ns/type.object.name> ?o3 .?s <http://rdf.freebase.com/ns/film.film.directed_by> ?o4 .?s <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o5 .?s <http://creativecommons.org/ns#attributionName> ?o6 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o7 .?o7 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .}"
				};
		
		SPARQL_Processor.main(allQueries);
	}
	
	@Test
	public void testGetRdfItems() {
		
		SPARQL_Parser testRdfParser = new SPARQL_Parser();
		
		//String query1 = "SELECT ?s, ?o1, ?o2 WHERE {?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o2 .}";
		//String query1 = "SELECT DISTINCT ?s, ?o8 WHERE { ?s <http://rdf.freebase.com/ns/common.topic.image> ?o1 . ?s <http://creativecommons.org/ns#attributionURL> ?o2 . ?s <http://rdf.freebase.com/ns/type.object.name> ?o3 . ?s <http://rdf.freebase.com/ns/film.film.directed_by> ?o4 . ?s <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o5 . ?s <http://creativecommons.org/ns#attributionName> ?o6 . ?s <http://rdf.freebase.com/ns/film.film.starring> ?o7 . ?o7 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .}";
		String query1 = "SELECT * WHERE { ?s <http://rdf.freebase.com/ns/common.topic.image> ?o1 . ?s <http://creativecommons.org/ns#attributionURL> ?o2 . ?s <http://rdf.freebase.com/ns/type.object.name> ?o3 . ?s <http://rdf.freebase.com/ns/film.film.directed_by> ?o4 . ?s <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o5 . ?s <http://creativecommons.org/ns#attributionName> ?o6 . ?s <http://rdf.freebase.com/ns/film.film.starring> ?o7 . ?o7 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .}";
		
		ArrayList<Triple<RdfItem, RdfItem, RdfItem>> testRdfItemList = testRdfParser.getRdfJoins(query1);		
		ArrayList<RdfItem> testRdfVars = testRdfParser.getRdfVariables(query1);

		boolean testIsDistinct = testRdfParser.isDistinct(query1);
		
		System.out.println(query1);
		
		System.out.println("Variables");
		
		for(RdfItem t : testRdfVars)
			System.out.println(t.getType() + ": " + t.getValue());		
		
		System.out.println("Joins");
		
		for(Triple<RdfItem, RdfItem, RdfItem> t : testRdfItemList)
			System.out.println(t.getSubject().getType() + ": " + t.getSubject().getValue() +
					", " + t.getPredicate().getType() + ": " + t.getPredicate().getValue() +
					", " + t.getObject().getType() + ": " + t.getObject().getValue());
		
		SparqlQuery testQuery = new SparqlQuery(testRdfVars, testRdfItemList, testIsDistinct);
		System.out.println(testQuery.toSQL());
		
	}
	
	@Test
	public void testGetRdfItemsC() throws ClassNotFoundException, IOException, SQLException {
		String query1 = "SELECT ?s, ?o1, ?o2 WHERE {?s asdt ?o1 .?o1 234fdf ?o2 .}";
		SPARQL_Processor.main(new String[]{ query1 });
	}
	
	@Test
	public void testGetRdfItemsEWarmUp() throws ClassNotFoundException, IOException, SQLException {
		String[] allQueries = new String[] { 
				"w",				
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o0 . ?o0 <http://rdf.freebase.com/ns/film.film.starring> ?o1 . ?o1 <http://rdf.freebase.com/ns/film.performance.film> ?o2 . }"};
		SPARQL_Processor.main(allQueries);		
	}
	
	@Test
	public void testGetRdfItemsE() throws ClassNotFoundException, IOException, SQLException {
		String[] allQueries = new String[] { 
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o0 . ?o0 <http://rdf.freebase.com/ns/film.film.starring> ?o1 . ?o1 <http://rdf.freebase.com/ns/film.performance.film> ?o2 .}"};
		SPARQL_Processor.main(allQueries);		
	}
	
	@Test
	public void testGetRdfItemsF() throws ClassNotFoundException, IOException, SQLException {
		String[] allQueries = new String[] { 
				"SELECT DISTINCT ?s WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o0 .?o0 <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?o1 <http://rdf.freebase.com/ns/film.performance.film> ?o2 .}"};
		SPARQL_Processor.main(allQueries);		
	}
	
	@Test
	public void testGetRdfItemsDWarmUp() throws ClassNotFoundException, IOException, SQLException {
		String[] allQueries = new String[] { 
				"w",				
				"SELECT ?s, ?o WHERE { ?s <http://rdf.freebase.com/ns/common.topic.webpage> ?o .}"};
		SPARQL_Processor.main(allQueries);		
	}
	
	@Test
	public void testGetRdfItemsD() throws ClassNotFoundException, IOException, SQLException {
		String[] allQueries = new String[] { 
				"SELECT ?s, ?o WHERE { ?s <http://rdf.freebase.com/ns/common.topic.webpage> ?o .}"};
		SPARQL_Processor.main(allQueries);		
	}
		
	@Test
	public void testGetRdfItems1W() throws ClassNotFoundException, IOException, SQLException {
		String query1 = "SELECT ?s, ?o WHERE { ?s <http://rdf.freebase.com/ns/common.topic.webpage> ?o .}";
		SPARQL_Processor.main(new String[]{ "w", query1 });		
	}
	
	@Test
	public void testGetRdfItems1All() throws ClassNotFoundException, IOException, SQLException {
		SPARQL_Processor.main(new String[]{ "-allSample", "1", "1" });		
	}
	
	@Test
	public void testGetRdfItems1() throws ClassNotFoundException, IOException, SQLException {
		String query1 = "SELECT ?s, ?o WHERE { ?s <http://rdf.freebase.com/ns/common.topic.webpage> ?o .}";
		SPARQL_Processor.main(new String[]{ query1 });		
	}
	
	@Test
	public void testGetRdfItems2() throws ClassNotFoundException, IOException, SQLException {		
		String query1 = "SELECT * WHERE { ?s <http://rdf.freebase.com/ns/location.location.geolocation> ?o .}";
		SPARQL_Processor.main(new String[]{ query1 });
	}
	
	@Test
	public void testGetRdfItems3() throws ClassNotFoundException, IOException, SQLException {		
		String query1 = "SELECT ?s, ?o1, ?o2 WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o2 .}";
		SPARQL_Processor.main(new String[]{ query1 });	
	}
	
	@Test
	public void testGetRdfItems4() throws ClassNotFoundException, IOException, SQLException {
		String query1 = "SELECT ?s, ?o1, ?o2 WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .}";
		SPARQL_Processor.main(new String[]{ query1 });
	}
	
	@Test
	public void testGetRdfItems5() throws ClassNotFoundException, IOException, SQLException {
		String query1 = "SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film.genre> ?o0 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?s <http://www.w3.org/2002/07/owl#sameAs> ?o2 .}";
		SPARQL_Processor.main(new String[]{ query1 });
	}
	
	@Test
	public void testGetRdfItems6() throws ClassNotFoundException, IOException, SQLException {		
		String query1 = "SELECT DISTINCT ?o1, ?o2 WHERE {?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o2 .?o2 <http://rdf.freebase.com/ns/film.performance.film> ?o3 .}";
		SPARQL_Processor.main(new String[]{ query1 });
	}
	
	@Test
	public void testGetRdfItems7() throws ClassNotFoundException, IOException, SQLException {
		String query1 = "SELECT ?s, ?o1, ?o2, ?o3, ?o4 WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o3 .?o3 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .}";
		SPARQL_Processor.main(new String[]{ query1 });
	}
	
	@Test
	public void testGetRdfItems8() throws ClassNotFoundException, IOException, SQLException {
		String query1 = "SELECT DISTINCT ?s, ?o5 WHERE { ?s <http://rdf.freebase.com/ns/common.topic.article> ?o1 .?s <http://creativecommons.org/ns#attributionName> ?o2 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o3 .?o3 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .?o4 <http://rdf.freebase.com/ns/film.film.starring> ?o5 .}";
		SPARQL_Processor.main(new String[]{ query1 });
	}
	
	@Test
	public void testGetRdfItems9() throws ClassNotFoundException, IOException, SQLException {
		String query1 = "SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o3 .?o3 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .?o4 <http://rdf.freebase.com/ns/film.film.starring> ?o5 .?o5 <http://rdf.freebase.com/ns/film.performance.film> ?o6 .?o6 <http://rdf.freebase.com/ns/film.film.starring> ?o7 .}";
		SPARQL_Processor.main(new String[]{ query1 });
	}
	
	@Test
	public void testGetRdfItems10() throws ClassNotFoundException, IOException, SQLException {
		String query1 = "SELECT DISTINCT ?s, ?o8 WHERE {?s <http://rdf.freebase.com/ns/common.topic.image> ?o1 .?s <http://creativecommons.org/ns#attributionURL> ?o2 .?s <http://rdf.freebase.com/ns/type.object.name> ?o3 .?s <http://rdf.freebase.com/ns/film.film.directed_by> ?o4 .?s <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o5 .?s <http://creativecommons.org/ns#attributionName> ?o6 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o7 .?o7 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .}";
		SPARQL_Processor.main(new String[]{ query1 });
	}
}
