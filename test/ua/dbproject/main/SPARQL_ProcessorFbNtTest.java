package ua.dbproject.main;
import java.io.IOException;
import java.sql.SQLException;
import org.junit.Test;
import ua.dbproject.main.SPARQL_Processor;

public class SPARQL_ProcessorFbNtTest {
	@Test
	public void testGetRdfItemsAll() throws ClassNotFoundException, IOException, SQLException {
		String[] allQueries = new String[] {
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o0 .?o0 <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?o1 <http://rdf.freebase.com/ns/film.performance.film> ?o2 .}",
				"SELECT DISTINCT ?s WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o0 .?o0 <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?o1 <http://rdf.freebase.com/ns/film.performance.film> ?o2 .}",
				"SELECT ?s, ?o1, ?o2 WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .}",
				"SELECT DISTINCT ?s WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .}",
				"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o4 .?o4 <http://rdf.freebase.com/ns/film.performance.film> ?o5 .}",//6
				"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .}",
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o0 .?o0 <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?o1 <http://rdf.freebase.com/ns/film.performance.film> ?o2 .}",
				"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o4 .?o4 <http://rdf.freebase.com/ns/film.performance.film> ?o5 .}",//9
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/common.topic.article> ?o0 .?s <http://creativecommons.org/ns#attributionName> ?o1 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o2 .}",
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film.starring> ?o0 .?o0 <http://rdf.freebase.com/ns/film.performance.film> ?o1 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o2 .}",
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/common.topic.article> ?o0 .?s <http://creativecommons.org/ns#attributionName> ?o1 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o2 .?o2 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .?o4 <http://rdf.freebase.com/ns/film.film.starring> ?o5 .}",
				"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/aviation.aircraft_owner> .?s <http://www.w3.org/2002/07/owl#sameAs> ?o1 .?s <http://rdf.freebase.com/ns/type.object.name> ?o2 .}",
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/common.topic.webpage> ?o0 .}",
				"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/aviation.aircraft_owner> .?s <http://www.w3.org/2002/07/owl#sameAs> ?o1 .?s <http://rdf.freebase.com/ns/type.object.name> ?o2 .?s <http://rdf.freebase.com/ns/common.topic.webpage> ?o3 .}",
				"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .}",
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o0 .?o0 <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?o1 <http://rdf.freebase.com/ns/film.performance.film> ?o2 .?o2 <http://rdf.freebase.com/ns/film.film.starring> ?o3 .?o3 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .?o4 <http://rdf.freebase.com/ns/film.film.starring> ?o5 .}",
				"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o4 .?o4 <http://rdf.freebase.com/ns/film.performance.film> ?o5 .?o5 <http://rdf.freebase.com/ns/film.film.starring> ?o6 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o7 .?o7 <http://rdf.freebase.com/ns/film.film.starring> ?o8 .}",
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film.genre> ?o0 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?s <http://www.w3.org/2002/07/owl#sameAs> ?o2 .}",
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film.starring> ?o0 .?o0 <http://rdf.freebase.com/ns/film.performance.film> ?o1 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o2 .?o2 <http://rdf.freebase.com/ns/film.performance.film> ?o3 .?o3 <http://rdf.freebase.com/ns/film.film.starring> ?o4 .?o4 <http://rdf.freebase.com/ns/film.performance.film> ?o5 .}",
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film.genre> ?o0 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?s <http://www.w3.org/2002/07/owl#sameAs> ?o2 .?o1 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .?o4 <http://rdf.freebase.com/ns/film.film.starring> ?o5 .?o5 <http://rdf.freebase.com/ns/film.performance.film> ?o6 .?o6 <http://rdf.freebase.com/ns/film.film.starring> ?o7 .?o7 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .}",
				"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/common.topic> .?s <http://rdf.freebase.com/ns/location.location.geolocation> ?o1 .?s <http://creativecommons.org/ns#attributionURL> ?o2 .?s <http://rdf.freebase.com/ns/type.object.name> ?o3 .?s <http://rdf.freebase.com/ns/location.location.containedby> ?o4 .}",
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/location.location.geolocation> ?o0 .}",
				"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/common.topic> .?s <http://rdf.freebase.com/ns/location.location.geolocation> ?o1 .?s <http://creativecommons.org/ns#attributionURL> ?o2 .?s <http://rdf.freebase.com/ns/type.object.name> ?o3 .?s <http://rdf.freebase.com/ns/location.location.containedby> ?o4 .?s <http://rdf.freebase.com/ns/type.object.name> ?o5 .}",
				"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .}",
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o0 .?o0 <http://rdf.freebase.com/ns/film.film.starring> ?o1 .}",
				"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o4 .}",
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/common.topic.image> ?o0 .?s <http://creativecommons.org/ns#attributionURL> ?o1 .?s <http://rdf.freebase.com/ns/type.object.name> ?o2 .?s <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o4 .?s <http://creativecommons.org/ns#attributionName> ?o5 .}",
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film.starring> ?o0 .?o0 <http://rdf.freebase.com/ns/film.performance.film> ?o1 .}",
				"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/common.topic.image> ?o0 .?s <http://creativecommons.org/ns#attributionURL> ?o1 .?s <http://rdf.freebase.com/ns/type.object.name> ?o2 .?s <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o4 .?s <http://creativecommons.org/ns#attributionName> ?o5 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o6 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .}",
				"SELECT * WHERE { ?s1 <http://rdf.freebase.com/ns/common.topic.image> ?o0 .?s1 <http://creativecommons.org/ns#attributionURL> ?o1 .?s1 <http://rdf.freebase.com/ns/type.object.name> ?o2 .?s1 <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s1 <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o4 .?s1 <http://creativecommons.org/ns#attributionName> ?o5 .?s1 <http://rdf.freebase.com/ns/film.film.starring> ?o6 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .?s2 <http://rdf.freebase.com/ns/common.topic.image> ?o9 .?s2 <http://creativecommons.org/ns#attributionURL> ?o10 .?s2 <http://rdf.freebase.com/ns/type.object.name> ?o11 .?s2 <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s2 <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o12 .?s2 <http://creativecommons.org/ns#attributionName> ?o13 .?s2 <http://rdf.freebase.com/ns/film.film.starring> ?o14 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o15 .}",
				"SELECT DISTINCT ?s1, ?s2, ?o8, ?o15 WHERE { ?s1 <http://rdf.freebase.com/ns/common.topic.image> ?o0 .?s1 <http://creativecommons.org/ns#attributionURL> ?o1 .?s1 <http://rdf.freebase.com/ns/type.object.name> ?o2 .?s1 <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s1 <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o4 .?s1 <http://creativecommons.org/ns#attributionName> ?o5 .?s1 <http://rdf.freebase.com/ns/film.film.starring> ?o6 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .?s2 <http://rdf.freebase.com/ns/common.topic.image> ?o9 .?s2 <http://creativecommons.org/ns#attributionURL> ?o10 .?s2 <http://rdf.freebase.com/ns/type.object.name> ?o11 .?s2 <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s2 <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o12 .?s2 <http://creativecommons.org/ns#attributionName> ?o13 .?s2 <http://rdf.freebase.com/ns/film.film.starring> ?o14 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o15 .}",
				"SELECT DISTINCT ?o8, ?o15 WHERE { ?s1 <http://rdf.freebase.com/ns/common.topic.image> ?o0 .?s1 <http://creativecommons.org/ns#attributionURL> ?o1 .?s1 <http://rdf.freebase.com/ns/type.object.name> ?o2 .?s1 <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s1 <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o4 .?s1 <http://creativecommons.org/ns#attributionName> ?o5 .?s1 <http://rdf.freebase.com/ns/film.film.starring> ?o6 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .?s2 <http://rdf.freebase.com/ns/common.topic.image> ?o9 .?s2 <http://creativecommons.org/ns#attributionURL> ?o10 .?s2 <http://rdf.freebase.com/ns/type.object.name> ?o11 .?s2 <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s2 <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o12 .?s2 <http://creativecommons.org/ns#attributionName> ?o13 .?s2 <http://rdf.freebase.com/ns/film.film.starring> ?o14 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o15 .}",
				};
		
		SPARQL_Processor.main(allQueries);
	}
	
	@Test
	public void testGetRdfItemsAllOut() throws ClassNotFoundException, IOException, SQLException {
		String query = "-all";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItemsAllSpecific() throws ClassNotFoundException, IOException, SQLException {
		String query = "-all";
		SPARQL_Processor.main(new String[]{ query, "2", "2" });
	}
	
	@Test
	public void testGetRdfItems1() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o0 .?o0 <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?o1 <http://rdf.freebase.com/ns/film.performance.film> ?o2 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems2() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT DISTINCT ?s WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o0 .?o0 <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?o1 <http://rdf.freebase.com/ns/film.performance.film> ?o2 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems3() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT ?s, ?o1, ?o2 WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems4() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT DISTINCT ?s WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems5() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o4 .?o4 <http://rdf.freebase.com/ns/film.performance.film> ?o5 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems6() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems7() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o0 .?o0 <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?o1 <http://rdf.freebase.com/ns/film.performance.film> ?o2 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems8() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o4 .?o4 <http://rdf.freebase.com/ns/film.performance.film> ?o5 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems9() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://rdf.freebase.com/ns/common.topic.article> ?o0 .?s <http://creativecommons.org/ns#attributionName> ?o1 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o2 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems10() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film.starring> ?o0 .?o0 <http://rdf.freebase.com/ns/film.performance.film> ?o1 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o2 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems11() throws ClassNotFoundException, IOException, SQLException {

		String query = "SELECT * WHERE { ?s <http://rdf.freebase.com/ns/common.topic.article> ?o0 .?s <http://creativecommons.org/ns#attributionName> ?o1 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o2 .?o2 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .?o4 <http://rdf.freebase.com/ns/film.film.starring> ?o5 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems12() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/aviation.aircraft_owner> .?s <http://www.w3.org/2002/07/owl#sameAs> ?o1 .?s <http://rdf.freebase.com/ns/type.object.name> ?o2 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems13() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://rdf.freebase.com/ns/common.topic.webpage> ?o0 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems14() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/aviation.aircraft_owner> .?s <http://www.w3.org/2002/07/owl#sameAs> ?o1 .?s <http://rdf.freebase.com/ns/type.object.name> ?o2 .?s <http://rdf.freebase.com/ns/common.topic.webpage> ?o3 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems15() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems16() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o0 .?o0 <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?o1 <http://rdf.freebase.com/ns/film.performance.film> ?o2 .?o2 <http://rdf.freebase.com/ns/film.film.starring> ?o3 .?o3 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .?o4 <http://rdf.freebase.com/ns/film.film.starring> ?o5 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems17() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o4 .?o4 <http://rdf.freebase.com/ns/film.performance.film> ?o5 .?o5 <http://rdf.freebase.com/ns/film.film.starring> ?o6 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o7 .?o7 <http://rdf.freebase.com/ns/film.film.starring> ?o8 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems18() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film.genre> ?o0 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?s <http://www.w3.org/2002/07/owl#sameAs> ?o2 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems19() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film.starring> ?o0 .?o0 <http://rdf.freebase.com/ns/film.performance.film> ?o1 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o2 .?o2 <http://rdf.freebase.com/ns/film.performance.film> ?o3 .?o3 <http://rdf.freebase.com/ns/film.film.starring> ?o4 .?o4 <http://rdf.freebase.com/ns/film.performance.film> ?o5 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems20() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film.genre> ?o0 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?s <http://www.w3.org/2002/07/owl#sameAs> ?o2 .?o1 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .?o4 <http://rdf.freebase.com/ns/film.film.starring> ?o5 .?o5 <http://rdf.freebase.com/ns/film.performance.film> ?o6 .?o6 <http://rdf.freebase.com/ns/film.film.starring> ?o7 .?o7 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems21() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/common.topic> .?s <http://rdf.freebase.com/ns/location.location.geolocation> ?o1 .?s <http://creativecommons.org/ns#attributionURL> ?o2 .?s <http://rdf.freebase.com/ns/type.object.name> ?o3 .?s <http://rdf.freebase.com/ns/location.location.containedby> ?o4 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems22() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://rdf.freebase.com/ns/location.location.geolocation> ?o0 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems23() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/common.topic> .?s <http://rdf.freebase.com/ns/location.location.geolocation> ?o1 .?s <http://creativecommons.org/ns#attributionURL> ?o2 .?s <http://rdf.freebase.com/ns/type.object.name> ?o3 .?s <http://rdf.freebase.com/ns/location.location.containedby> ?o4 .?s <http://rdf.freebase.com/ns/type.object.name> ?o5 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems24() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems25() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o0 .?o0 <http://rdf.freebase.com/ns/film.film.starring> ?o1 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems26() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o4 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems27() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://rdf.freebase.com/ns/common.topic.image> ?o0 .?s <http://creativecommons.org/ns#attributionURL> ?o1 .?s <http://rdf.freebase.com/ns/type.object.name> ?o2 .?s <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o4 .?s <http://creativecommons.org/ns#attributionName> ?o5 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems28() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film.starring> ?o0 .?o0 <http://rdf.freebase.com/ns/film.performance.film> ?o1 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems29() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s <http://rdf.freebase.com/ns/common.topic.image> ?o0 .?s <http://creativecommons.org/ns#attributionURL> ?o1 .?s <http://rdf.freebase.com/ns/type.object.name> ?o2 .?s <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o4 .?s <http://creativecommons.org/ns#attributionName> ?o5 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o6 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems30() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT * WHERE { ?s1 <http://rdf.freebase.com/ns/common.topic.image> ?o0 .?s1 <http://creativecommons.org/ns#attributionURL> ?o1 .?s1 <http://rdf.freebase.com/ns/type.object.name> ?o2 .?s1 <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s1 <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o4 .?s1 <http://creativecommons.org/ns#attributionName> ?o5 .?s1 <http://rdf.freebase.com/ns/film.film.starring> ?o6 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .?s2 <http://rdf.freebase.com/ns/common.topic.image> ?o9 .?s2 <http://creativecommons.org/ns#attributionURL> ?o10 .?s2 <http://rdf.freebase.com/ns/type.object.name> ?o11 .?s2 <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s2 <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o12 .?s2 <http://creativecommons.org/ns#attributionName> ?o13 .?s2 <http://rdf.freebase.com/ns/film.film.starring> ?o14 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o15 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems31() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT DISTINCT ?s1, ?s2, ?o8, ?o15 WHERE { ?s1 <http://rdf.freebase.com/ns/common.topic.image> ?o0 .?s1 <http://creativecommons.org/ns#attributionURL> ?o1 .?s1 <http://rdf.freebase.com/ns/type.object.name> ?o2 .?s1 <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s1 <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o4 .?s1 <http://creativecommons.org/ns#attributionName> ?o5 .?s1 <http://rdf.freebase.com/ns/film.film.starring> ?o6 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .?s2 <http://rdf.freebase.com/ns/common.topic.image> ?o9 .?s2 <http://creativecommons.org/ns#attributionURL> ?o10 .?s2 <http://rdf.freebase.com/ns/type.object.name> ?o11 .?s2 <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s2 <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o12 .?s2 <http://creativecommons.org/ns#attributionName> ?o13 .?s2 <http://rdf.freebase.com/ns/film.film.starring> ?o14 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o15 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
	
	@Test
	public void testGetRdfItems32() throws ClassNotFoundException, IOException, SQLException {
		String query = "SELECT DISTINCT ?o8, ?o15 WHERE { ?s1 <http://rdf.freebase.com/ns/common.topic.image> ?o0 .?s1 <http://creativecommons.org/ns#attributionURL> ?o1 .?s1 <http://rdf.freebase.com/ns/type.object.name> ?o2 .?s1 <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s1 <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o4 .?s1 <http://creativecommons.org/ns#attributionName> ?o5 .?s1 <http://rdf.freebase.com/ns/film.film.starring> ?o6 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .?s2 <http://rdf.freebase.com/ns/common.topic.image> ?o9 .?s2 <http://creativecommons.org/ns#attributionURL> ?o10 .?s2 <http://rdf.freebase.com/ns/type.object.name> ?o11 .?s2 <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s2 <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o12 .?s2 <http://creativecommons.org/ns#attributionName> ?o13 .?s2 <http://rdf.freebase.com/ns/film.film.starring> ?o14 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o15 .}";
		SPARQL_Processor.main(new String[]{ query });
	}
}
