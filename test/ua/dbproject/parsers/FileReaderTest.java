package ua.dbproject.parsers;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Test;

import ua.dbproject.dictionary.DictUtility;

public class FileReaderTest {

	@Test
	public void testReadFile() throws FileNotFoundException {
		FileHandler.deleteFile("asdf.txt");
		FileHandler.writeFile("asdf", "asdf.txt");
		String strRead = FileHandler.readFile("asdf.txt");
		assertEquals("Asserting 'asdf' string on FileReader failed", strRead, "asdf");
		FileHandler.deleteFile("asdf.txt");
	}
	
	@Test (expected=FileNotFoundException.class)
	public void testReadFileNotFound() throws FileNotFoundException {
		FileHandler.readFile("doesn'tExist.txt");
	}

	@Test
	public void testWriteFile() throws ClassNotFoundException, IOException {
		FileHandler.writeFile("asdf", DictUtility.getCachePath("test1.txt"));
		FileHandler.deleteFile(DictUtility.getCachePath("test1.txt"));
	}

}
