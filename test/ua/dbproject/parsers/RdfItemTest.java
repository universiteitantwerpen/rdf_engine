package ua.dbproject.parsers;

import static org.junit.Assert.*;

import org.junit.Test;

public class RdfItemTest {

	@Test
	public void testGetValueConstant() {
		RdfItem testItem = new RdfItem("<asdf>");
		
		assertEquals("Should be equal to <asdf>", "<asdf>", testItem.getRawValue());
		assertEquals("Should be type CONSTANT", RdfItemType.CONSTANT, testItem.getType());
		assertEquals("Should be equal to asdf", "asdf", testItem.getValue());
		
		System.out.println(testItem.getRawValue() + ", " + testItem.getType() + ", " + testItem.getValue());
		
	}
	
	@Test
	public void testGetValueVariable() {
		RdfItem testItem = new RdfItem("?asdfVar");
		
		assertEquals("Should be equal to ?asdf", "?asdfVar", testItem.getRawValue());
		assertEquals("Should be type VARIABLE", RdfItemType.VARIABLE, testItem.getType());
		assertEquals("Should be equal to asdfVar", "asdfVar", testItem.getValue());
		
		System.out.println(testItem.getRawValue() + ", " + testItem.getType() + ", " + testItem.getValue());
	}
}
