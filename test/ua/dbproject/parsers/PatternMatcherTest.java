package ua.dbproject.parsers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.Test;

public class PatternMatcherTest {
	
	@Test
	public void testMatch3ABCD() {
		
		String[] lines = new String[] {
			"<http://rdf.freebase.com/ns/m.0cgskpc> \"103.0\"^^<http://www.w3.org/2001/XMLSchema#float> \"Nieuw-Buinen\"@as .", 
			"\"Andouque\"@en <http://rdf.freebase.com/ns/en.tarn> <http://rdf.freebase.com/ns/m.03npm90> .",
			"<http://rdf.freebase.com/ns/m.0d95sb0> <http://www.freebase.com/view/m/03npm7n> \"asfd\"@ja .",
			"_:httpx3Ax2Fx2Frdfx2Efreebasex2Ecomx2Frdfx2Fmx2F09hgzhxxbnode2 <http://rdf.freebase.com/ns/type.value.value> \"246633\" .",
			"_:header9091218851335191302645 <http://www.w3.org/2006/http#content-type> \"application/rdf+xml\" .",
			"<http://rdf.freebase.com/ns/m.0672d> <http://rdf.freebase.com/ns/type.object.name> \"\u4EE5\u8272\u5217\u7E3D\u7406\"@zh-hant .",
			"<http://rdf.freebase.com/ns/m.0672d> <http://rdf.freebase.com/ns/type.object.name> \"Primer ministro de Israel\"@es-419 .",
		};
		
		Pattern pattern = Pattern.compile("((\".*\"\\^\\^.*>)|<(.*)>|_:(.*)[1-9]|\"(.*)\" |(\".*\"\\@[A-Za-z\\-0-9]* .*)).*"
				+ "((\".*\"\\^\\^.*>)|<(.*)>|_:(.*)[1-9]|\"(.*)\" |(\".*\"\\@[A-Za-z\\-0-9]* .*)).*"
				+ "((\".*\"\\^\\^.*>)|<(.*)>|_:(.*)[1-9]|\"(.*)\" |(\".*\"\\@[A-Za-z\\-0-9]* .*)).*\\.");
		
		for (String line : lines) {
			Matcher matcher = pattern.matcher(line);
		    
			System.out.println("Original: " + line);
			System.out.print("Match: ");
			
		    if (matcher.matches()) {		    	
		    	System.out.println(matcher.group(1) + "\t" + matcher.group(7) + "\t" + matcher.group(13));// + "\t" + matcher.group(3));// + "\t" + matcher.group(4));		    	
		    }
		}
	}
	
	@Test
	public void testMultiABC() {
		
		int count = 3;
		
		String strPattern = "";
		String[] lines = new String[] {
				"<http://rdf.freebase.com/ns/m.0cgskpc> \"Nieuw-Buinen\"@as", 
				"\"Andouque\"@en <http://rdf.freebase.com/ns/en.tarn>",
				
				"\"103.0\"^^<http://www.w3.org/2001/XMLSchema#float> <http://rdf.freebase.com/ns/m.0cmyh1p>",
				"<http://rdf.freebase.com/ns/m.0cmyh1p> \"103.0\"^^<http://www.w3.org/2001/XMLSchema#float>",

				"\"asdf\" <http://rdf.freebase.com/ns/m.0d95sb0>",
				"<http://rdf.freebase.com/ns/m.0d95sb0> \"asdf\"",

				"<http://www.w3.org/2001/XMLSchema#float> <http://rdf.freebase.com/ns/m.0d95sb0>",
				"<http://rdf.freebase.com/ns/m.0d95sb0> <http://www.w3.org/2001/XMLSchema#float>",
				
				"<http://rdf.freebase.com/ns/m.0cgskpc> \"103.0\"^^<http://www.w3.org/2001/XMLSchema#float> \"Nieuw-Buinen\"@as", 
				"\"Andouque\"@en <http://rdf.freebase.com/ns/en.tarn> <http://rdf.freebase.com/ns/m.03npm90>",
				"<http://rdf.freebase.com/ns/m.0d95sb0> <http://www.freebase.com/view/m/03npm7n> \"asfd\"@ja", 
			};
		
		strPattern += "((\".*\"\\^\\^.*>)|<(.*)>|\"(.*)\"|(\".*\"\\@[A-Za-z][A-Za-z].*))";		
		for (int i = 1; i < count; i++) {
			strPattern += " ((\".*\"\\^\\^.*>)|<(.*)>|\"(.*)\"|(\".*\"\\@[A-Za-z][A-Za-z].*))";
		}

		Pattern pattern = Pattern.compile(strPattern); 
			
		for (String line : lines) {
			Matcher matcher = pattern.matcher(line);
		    
			System.out.println("Original: " + line);
			System.out.print("Match: ");
			

		    if (matcher.matches()) {
		    	
		    	System.out.println(matcher.group(1) + "\t" + matcher.group(6) + "\t" + matcher.group(11));// + "\t" + matcher.group(4));
		    	
		    }
		}
	}
	
	@Test
	public void testMatchA() {
		
		String[] lines = new String[] {
			"\"95.0\"^^<http://www.w3.org/2001/XMLSchema#float>",
			"\"104.0\"^^<http://www.w3.org/2001/XMLSchema#float>",
			"\"103.0\"^^<http://www.w3.org/2001/XMLSchema#float>",
			"<http://www.w3.org/2001/XMLSchema#float>"
		};
		
//		Pattern pattern = Pattern.compile(".*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*.*([<\"])(.*)([>\"]).*"); //4
//		Pattern pattern = Pattern.compile(".*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*"); //3
//		Pattern pattern = Pattern.compile(".*([<\"])(.*)(^^)(.*)([>\"])"); //3
//		Pattern pattern = Pattern.compile("(.*)(^^).*([<\"])(.*)([>\"])"); //3
//		Pattern pattern = Pattern.compile(".*(\".*\"\\^\\^.*<.*>)"); 
		
		//(?|(a)|(b)|(c))
//		Pattern pattern = Pattern.compile(".*((\".*\"\\^\\^.*<.*>)|<(.*)>)"); 
//		Pattern pattern = Pattern.compile("(\".*\".*>)"); 
//		Pattern pattern = Pattern.compile("(\".*\"\\^\\^.*>)"); 
		Pattern pattern = Pattern.compile("((\".*\"\\^\\^.*>)|<(.*)>)"); 
		
		for (String line : lines) {
			Matcher matcher = pattern.matcher(line);
		    
			System.out.println("Original: " + line);
			System.out.print("Match: ");
			
		    if (matcher.matches()) {
//		    	System.out.println(matcher.group(1) + "\t" + matcher.group(2) + "\t" + matcher.group(3) + "\t" + matcher.group(4) + "\t" + 
//		    			matcher.group(5) + "\t" + matcher.group(6) + "\t" + matcher.group(7) + "\t" + matcher.group(8) + "\t" + matcher.group(9) + 
//		    			"\t" + matcher.group(10));//1
//		    	System.out.println(matcher.group(2) + "\t" + matcher.group(5) + "\t" + matcher.group(8));//1
		    	
		    	System.out.println(matcher.group(1));// + "\t" + matcher.group(2));// + "\t" + matcher.group(3));// + "\t" + matcher.group(4));
		    	
		    }
		}
	}
	
	@Test
	public void testMatchAB() {
		
		String[] lines = new String[] {
			"\"95.0\"^^<http://www.w3.org/2001/XMLSchema#float>",
			"\"104.0\"^^<http://www.w3.org/2001/XMLSchema#float>",
			"\"103.0\"^^<http://www.w3.org/2001/XMLSchema#float>",
			"<http://www.w3.org/2001/XMLSchema#float>",
			"<http://rdf.freebase.com/ns/m.0410wnz>",
			"\"Source: Freebase - The World's database\"",
			"<http://rdf.freebase.com/ns/m.0cgskpc>", 
		};
		
//		Pattern pattern = Pattern.compile(".*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*.*([<\"])(.*)([>\"]).*"); //4
//		Pattern pattern = Pattern.compile(".*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*"); //3
//		Pattern pattern = Pattern.compile(".*([<\"])(.*)(^^)(.*)([>\"])"); //3
//		Pattern pattern = Pattern.compile("(.*)(^^).*([<\"])(.*)([>\"])"); //3
//		Pattern pattern = Pattern.compile(".*(\".*\"\\^\\^.*<.*>)"); 
		
		//(?|(a)|(b)|(c))
//		Pattern pattern = Pattern.compile(".*((\".*\"\\^\\^.*<.*>)|<(.*)>)"); 
//		Pattern pattern = Pattern.compile("(\".*\".*>)"); 
//		Pattern pattern = Pattern.compile("(\".*\"\\^\\^.*>)"); 
		Pattern pattern = Pattern.compile("((\".*\"\\^\\^.*>)|<(.*)>|\"(.*)\")");
		
		for (String line : lines) {
			Matcher matcher = pattern.matcher(line);
		    
			System.out.println("Original: " + line);
			System.out.print("Match: ");
		    if (matcher.matches()) {
//		    	System.out.println(matcher.group(1) + "\t" + matcher.group(2) + "\t" + matcher.group(3) + "\t" + matcher.group(4) + "\t" + 
//		    			matcher.group(5) + "\t" + matcher.group(6) + "\t" + matcher.group(7) + "\t" + matcher.group(8) + "\t" + matcher.group(9) + 
//		    			"\t" + matcher.group(10));//1
//		    	System.out.println(matcher.group(2) + "\t" + matcher.group(5) + "\t" + matcher.group(8));//1
		    	
		    	System.out.println(matcher.group(1));// + "\t" + matcher.group(2));// + "\t" + matcher.group(3));// + "\t" + matcher.group(4));
		    	
		    }
		}
	}
	
	@Test
	public void testMatch3ABC() {
		
		String[] lines = new String[] {
			"\"95.0\"^^<http://www.w3.org/2001/XMLSchema#float>",
			"\"104.0\"^^<http://www.w3.org/2001/XMLSchema#float>",
			"\"103.0\"^^<http://www.w3.org/2001/XMLSchema#float>",
			"<http://www.w3.org/2001/XMLSchema#float>",
			"<http://rdf.freebase.com/ns/m.0410wnz>",
			"\"Source: Freebase - The World's database\"",
			"<http://rdf.freebase.com/ns/m.0cgskpc> \"103.0\"^^<http://www.w3.org/2001/XMLSchema#float> \"Nieuw-Buinen\"@as .", 
			"\"Andouque\"@en <http://rdf.freebase.com/ns/en.tarn> <http://rdf.freebase.com/ns/m.03npm90> .",
			"<http://rdf.freebase.com/ns/m.0d95sb0> <http://www.freebase.com/view/m/03npm7n> \"asfd\"@ja .", 
		};
		
//		Pattern pattern = Pattern.compile(".*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*.*([<\"])(.*)([>\"]).*"); //4
//		Pattern pattern = Pattern.compile(".*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*"); //3
//		Pattern pattern = Pattern.compile(".*([<\"])(.*)(^^)(.*)([>\"])"); //3
//		Pattern pattern = Pattern.compile("(.*)(^^).*([<\"])(.*)([>\"])"); //3
//		Pattern pattern = Pattern.compile(".*(\".*\"\\^\\^.*<.*>)"); 
		
		//(?|(a)|(b)|(c))
//		Pattern pattern = Pattern.compile(".*((\".*\"\\^\\^.*<.*>)|<(.*)>)"); 
//		Pattern pattern = Pattern.compile("(\".*\".*>)"); 
		Pattern pattern = Pattern.compile("((\".*\"\\^\\^.*>)|<(.*)>|\"(.*)\" |(\".*\"\\@[A-Za-z][A-Za-z] .*)).*"
				+ "((\".*\"\\^\\^.*>)|<(.*)>|\"(.*)\" |(\".*\"\\@[A-Za-z][A-Za-z] .*)).*"
				+ "((\".*\"\\^\\^.*>)|<(.*)>|\"(.*)\" |(\".*\"\\@[A-Za-z][A-Za-z] .*)).*\\.");
		
		for (String line : lines) {
			Matcher matcher = pattern.matcher(line);
		    
			System.out.println("Original: " + line);
			System.out.print("Match: ");
		    if (matcher.matches()) {
//		    	System.out.println(matcher.group(1) + "\t" + matcher.group(2) + "\t" + matcher.group(3) + "\t" + matcher.group(4) + "\t" + 
//		    			matcher.group(5) + "\t" + matcher.group(6) + "\t" + matcher.group(7) + "\t" + matcher.group(8) + "\t" + matcher.group(9) + 
//		    			"\t" + matcher.group(10));//1
//		    	System.out.println(matcher.group(2) + "\t" + matcher.group(5) + "\t" + matcher.group(8));//1
		    	
		    	System.out.println(matcher.group(1) + "\t" + matcher.group(6) + "\t" + matcher.group(11));// + "\t" + matcher.group(3));// + "\t" + matcher.group(4));
		    	
		    }
		}
	}
	
	@Test
	public void testMatch3ABC2() {
		
		String[] lines = new String[] {
			"\"95.0\"^^<http://www.w3.org/2001/XMLSchema#float>",
			"\"104.0\"^^<http://www.w3.org/2001/XMLSchema#float>",
			"\"103.0\"^^<http://www.w3.org/2001/XMLSchema#float>",
			"<http://www.w3.org/2001/XMLSchema#float>",
			"<http://rdf.freebase.com/ns/m.0410wnz>",
			"\"Source: Freebase - The World's database\"",
			"<http://rdf.freebase.com/ns/m.0cgskpc> \"103.0\"^^<http://www.w3.org/2001/XMLSchema#float> \"Nieuw-Buinen\"@as .", 
			"\"Nieuw-Buinen\"@en",
		};
		
//		Pattern pattern = Pattern.compile(".*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*.*([<\"])(.*)([>\"]).*"); //4
//		Pattern pattern = Pattern.compile(".*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*"); //3
//		Pattern pattern = Pattern.compile(".*([<\"])(.*)(^^)(.*)([>\"])"); //3
//		Pattern pattern = Pattern.compile("(.*)(^^).*([<\"])(.*)([>\"])"); //3
//		Pattern pattern = Pattern.compile(".*(\".*\"\\^\\^.*<.*>)"); 
		
		//(?|(a)|(b)|(c))
//		Pattern pattern = Pattern.compile(".*((\".*\"\\^\\^.*<.*>)|<(.*)>)"); 
//		Pattern pattern = Pattern.compile("(\".*\".*>)"); 
		Pattern pattern = Pattern.compile("((\".*\"\\^\\^.*>)|<(.*)>|\"(.*)\" |(\".*\"\\@[A-Za-z][A-Za-z] .*)) "
				+ "((\".*\"\\^\\^.*>)|<(.*)>|\"(.*)\" |(\".*\"\\@[A-Za-z][A-Za-z] .*)) "
				+ "((\".*\"\\^\\^.*>)|<(.*)>|\"(.*)\" |(\".*\"\\@[A-Za-z][A-Za-z] .*)).*\\.");
		
		for (String line : lines) {
			Matcher matcher = pattern.matcher(line);
		    
			System.out.println("Original: " + line);
			System.out.print("Match: ");
		    if (matcher.matches()) {
//		    	System.out.println(matcher.group(1) + "\t" + matcher.group(2) + "\t" + matcher.group(3) + "\t" + matcher.group(4) + "\t" + 
//		    			matcher.group(5) + "\t" + matcher.group(6) + "\t" + matcher.group(7) + "\t" + matcher.group(8) + "\t" + matcher.group(9) + 
//		    			"\t" + matcher.group(10));//1
//		    	System.out.println(matcher.group(2) + "\t" + matcher.group(5) + "\t" + matcher.group(8));//1
		    	
		    	System.out.println(matcher.group(1) + "\t" + matcher.group(6) + "\t" + matcher.group(11));// + "\t" + matcher.group(3));// + "\t" + matcher.group(4));
		    	
		    }
		}
	}
	
	@Test
	public void testMatch3ABC1() {
		
		String[] lines = new String[] {
			"\"95.0\"^^<http://www.w3.org/2001/XMLSchema#float>",
			"\"104.0\"^^<http://www.w3.org/2001/XMLSchema#float>",
			"\"103.0\"^^<http://www.w3.org/2001/XMLSchema#float>",
			"<http://www.w3.org/2001/XMLSchema#float>",
			"<http://rdf.freebase.com/ns/m.0410wnz>",
			"\"Source: Freebase - The World's database\"",
			"<http://rdf.freebase.com/ns/m.0cgskpc> <hasdfc> <lala>", 
			"\"Nieuw-Buinen\"@en",
			"<http://rdf.freebase.com/ns/m.0d95sb0> <http://www.freebase.com/view/m/03npm7n> \"asdf\"@ja",
		};
		
//		Pattern pattern = Pattern.compile(".*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*.*([<\"])(.*)([>\"]).*"); //4
//		Pattern pattern = Pattern.compile(".*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*"); //3
//		Pattern pattern = Pattern.compile(".*([<\"])(.*)(^^)(.*)([>\"])"); //3
//		Pattern pattern = Pattern.compile("(.*)(^^).*([<\"])(.*)([>\"])"); //3
//		Pattern pattern = Pattern.compile(".*(\".*\"\\^\\^.*<.*>)"); 
		
		//(?|(a)|(b)|(c))
//		Pattern pattern = Pattern.compile(".*((\".*\"\\^\\^.*<.*>)|<(.*)>)"); 
//		Pattern pattern = Pattern.compile("(\".*\".*>)"); 
		Pattern pattern = Pattern.compile("((\".*\"\\^\\^.*>)|<(.*)>|\"(.*)\"|(\".*\"\\@[A-Za-z][A-Za-z].*)) "
				+ "((\".*\"\\^\\^.*>)|<(.*)>|\"(.*)\"|(\".*\"\\@[A-Za-z][A-Za-z].*)) "
				+ "((\".*\"\\^\\^.*>)|<(.*)>|\"(.*)\"|(\".*\"\\@[A-Za-z][A-Za-z].*))");
		
		for (String line : lines) {
			Matcher matcher = pattern.matcher(line);
		    
			System.out.println("Original: " + line);
			System.out.print("Match: ");
		    if (matcher.matches()) {
//		    	System.out.println(matcher.group(1) + "\t" + matcher.group(2) + "\t" + matcher.group(3) + "\t" + matcher.group(4) + "\t" + 
//		    			matcher.group(5) + "\t" + matcher.group(6) + "\t" + matcher.group(7) + "\t" + matcher.group(8) + "\t" + matcher.group(9) + 
//		    			"\t" + matcher.group(10));//1
//		    	System.out.println(matcher.group(2) + "\t" + matcher.group(5) + "\t" + matcher.group(8));//1
		    	
		    	System.out.println(matcher.group(1) + "\t" + matcher.group(6) + "\t" + matcher.group(11));// + "\t" + matcher.group(3));// + "\t" + matcher.group(4));
		    	
		    }
		}
	}

	@Test
	public void testMatchABC() {
		
		String[] lines = new String[] {
			"\"95.0\"^^<http://www.w3.org/2001/XMLSchema#float>",
			"\"104.0\"^^<http://www.w3.org/2001/XMLSchema#float>",
			"\"103.0\"^^<http://www.w3.org/2001/XMLSchema#float>",
			"<http://www.w3.org/2001/XMLSchema#float>",
			"<http://rdf.freebase.com/ns/m.0410wnz>",
			"\"Source: Freebase - The World's database\"",
			"<http://rdf.freebase.com/ns/m.0cgskpc>", 
			"\"Nieuw-Buinen\"@en",
		};
		
//		Pattern pattern = Pattern.compile(".*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*.*([<\"])(.*)([>\"]).*"); //4
//		Pattern pattern = Pattern.compile(".*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*"); //3
//		Pattern pattern = Pattern.compile(".*([<\"])(.*)(^^)(.*)([>\"])"); //3
//		Pattern pattern = Pattern.compile("(.*)(^^).*([<\"])(.*)([>\"])"); //3
//		Pattern pattern = Pattern.compile(".*(\".*\"\\^\\^.*<.*>)"); 
		
		//(?|(a)|(b)|(c))
//		Pattern pattern = Pattern.compile(".*((\".*\"\\^\\^.*<.*>)|<(.*)>)"); 
//		Pattern pattern = Pattern.compile("(\".*\".*>)"); 
//		Pattern pattern = Pattern.compile("(\".*\"\\^\\^.*>)"); 
		Pattern pattern = Pattern.compile("((\".*\"\\^\\^.*>)|<(.*)>|\"(.*)\"|(\".*\"\\@[A-Za-z][A-Za-z].*))");
		
		for (String line : lines) {
			Matcher matcher = pattern.matcher(line);
		    
			System.out.println("Original: " + line);
			System.out.print("Match: ");
		    if (matcher.matches()) {
//		    	System.out.println(matcher.group(1) + "\t" + matcher.group(2) + "\t" + matcher.group(3) + "\t" + matcher.group(4) + "\t" + 
//		    			matcher.group(5) + "\t" + matcher.group(6) + "\t" + matcher.group(7) + "\t" + matcher.group(8) + "\t" + matcher.group(9) + 
//		    			"\t" + matcher.group(10));//1
//		    	System.out.println(matcher.group(2) + "\t" + matcher.group(5) + "\t" + matcher.group(8));//1
		    	
		    	System.out.println(matcher.group(1));// + "\t" + matcher.group(2));// + "\t" + matcher.group(3));// + "\t" + matcher.group(4));
		    	
		    }
		}
	}
	
	
	@Test
	public void testMatchA2() {
		
		String[] lines = new String[] {
			"<http://rdf.freebase.com/ns/m.0bs0wg0> \"95.0\"^^<http://www.w3.org/2001/XMLSchema#float> <http://rdf.freebase.com/ns/m.0cs67r3>",
			"<http://rdf.freebase.com/ns/m.0bs0wg0> \"104.0\"^^<http://www.w3.org/2001/XMLSchema#float> <http://rdf.freebase.com/ns/m.0bjbrk1>",
			"<http://rdf.freebase.com/ns/m.0bs0wg0> \"103.0\"^^<http://www.w3.org/2001/XMLSchema#float> <http://rdf.freebase.com/ns/m.0cmyh1p>",
		};
		
//		Pattern pattern = Pattern.compile(".*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*.*([<\"])(.*)([>\"]).*"); //4
//		Pattern pattern = Pattern.compile(".*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*"); //3
		Pattern pattern = Pattern.compile(".*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*([<\"])(.*)([>\"]).*"); //3
		
		for (String line : lines) {
			Matcher matcher = pattern.matcher(line);
		    if (matcher.matches()) {
		    	System.out.println(matcher.group(2) + "\t" + matcher.group(5) + "\t" + matcher.group(8));//1
		    }
		}
	}
}
