package ua.dbproject.parsers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

import ua.dbproject.indexer.Triple;

public class SPARQL_ParserTest {

	@Test
	public void testParse() {
		SPARQL_Parser asdf = new SPARQL_Parser();
		
		String query1 = "SELECT ?x ?y ?z ?member WHERE { ?x  <ouf>  JohnSmith. ?z ?y ?x }";
		
		System.out.println(asdf.getVariables(query1));
		
		for(Triple<String, String, String> t : asdf.getJoins(query1))
			System.out.println(t.getSubject() + ", " + t.getPredicate() + ", " + t.getObject());
	}
	
	@Test
	public void testRegex() {
		String line = "\"http://rdf.freebase.com/ns/m.09v1rt8\" <http://www.w3.org/2002/07/owl#sameAs> <http://dbpedia.org/resource/Daddy's_Gone_A-Hunting_(1969_film)> .";
		
		if (line.matches(".*(\\<).*(\\>).*(\\<).*(\\>).*(\\<).*(\\>).*(\\.)")) {
			System.out.println("ok");			
		}
		 
		Pattern pattern = Pattern.compile(".*([<\"])(.*)([>\"]).*(\\<)(.*)(\\>).*(\\<)(.*)(\\>).*(\\.)");  
	    Matcher matcher = pattern.matcher(line);
	    if (matcher.matches()) {
	    	System.out.println("ok2");
	    	System.out.println(matcher.group(2));
	    	System.out.println(matcher.group(5));
	    	System.out.println(matcher.group(8));
	    }
	}

}
