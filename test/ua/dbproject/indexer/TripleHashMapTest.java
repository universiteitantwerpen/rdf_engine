package ua.dbproject.indexer;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.junit.Test;

import ua.dbproject.dictionary.DictUtility;
import ua.dbproject.optimizer.StopWatch;
import ua.dbproject.parsers.FileHandler;
import ua.dbproject.serializer.ObjectSerializerGZIP;
import ua.dbproject.statistics.RuntimeStats;

public class TripleHashMapTest {
	
	private ObjectSerializerGZIP objectSerializer = ObjectSerializerGZIP.getInstance();

	@Test
	public void testStoreAllTriples() throws ClassNotFoundException, IOException {
		HashSet<Triple<Integer, Integer, Integer>> testHashSet = new HashSet<Triple<Integer, Integer, Integer>>();
		testHashSet.add(new Triple<Integer, Integer, Integer>(1, 2, 3));
		testHashSet.add(new Triple<Integer, Integer, Integer>(1, 2, 5));
		
		objectSerializer.serializeTripleHashSet(testHashSet, DictUtility.getCachePath("testSerializedTripleHashSet5"));
		HashSet<Triple<Integer, Integer, Integer>> hashSetResult2 = objectSerializer.deSerializeTripleHashSet(DictUtility.getCachePath("testSerializedTripleHashSet5"));
		for (Triple<Integer, Integer, Integer> currentTriple : hashSetResult2)
			System.out.println(currentTriple.getSubject() + " " + currentTriple.getPredicate() + " " + currentTriple.getObject());
		FileHandler.deleteFile(DictUtility.getCachePath("testSerializedTripleHashSet5"));
	}
	
	@Test
	public void testStoreTripleMap() throws ClassNotFoundException, IOException {
		Map<Integer, String> intToString = new HashMap<Integer, String>();
		
		intToString.put("asdf".hashCode(), "asdf");
		intToString.put("asdf2".hashCode(), "asdf2");
		
		objectSerializer.serializeHashDictionary(intToString, DictUtility.getCachePath("testSerializedTripleHashMap6"));
		
		Map<Integer, String> testMap = objectSerializer.deserializeHashDictionary(DictUtility.getCachePath("testSerializedTripleHashMap6"));
		System.out.println(testMap);
		FileHandler.deleteFile(DictUtility.getCachePath("testSerializedTripleHashMap6"));
	}
	
	@Test
	public void testReadAllTriplesWithTripleHashMap() throws ClassNotFoundException, IOException {
		
		Map<Integer, String> testMap = new HashMap<Integer, String>();
				
		HashSet<Triple<Integer, Integer, Integer>> testHashSet = new HashSet<Triple<Integer, Integer, Integer>>();
		testHashSet.add(new Triple<Integer, Integer, Integer>("asdf".hashCode(), "asdf2".hashCode(), "lala".hashCode()));
		testHashSet.add(new Triple<Integer, Integer, Integer>("asdf".hashCode(), "asdf2".hashCode(), "hmmm".hashCode()));
				
		testMap.put("asdf".hashCode(), "asdf");
		testMap.put("asdf2".hashCode(), "asdf2");
		testMap.put("lala".hashCode(), "lala");
		testMap.put("hmmm".hashCode(), "hmmm");
		
		objectSerializer.serializeHashDictionary(testMap, DictUtility.getCachePath("testSerializedTripleHashMap2"));
		Map<Integer, String> intToString = objectSerializer.deserializeHashDictionary(DictUtility.getCachePath("testSerializedTripleHashMap2"));
		
		System.out.println(intToString);
		
		objectSerializer.serializeTripleHashSet(testHashSet, DictUtility.getCachePath("testSerializedTripleHashSet"));
		HashSet<Triple<Integer, Integer, Integer>> hashSetResult2 = objectSerializer.deSerializeTripleHashSet(DictUtility.getCachePath("testSerializedTripleHashSet"));
		
		
		//HashSet<Triple<Integer, Integer, Integer>> hashSetResult2 = objectSerializer.deSerializeTripleHashSet(DictUtility.getPathToSave("testSerializedTripleHashSet"));
		for (Triple<Integer, Integer, Integer> currentTriple : hashSetResult2)
		{
			System.out.println(currentTriple.getSubject() + " " + currentTriple.getPredicate() + " " + currentTriple.getObject());
			System.out.println(intToString.get(currentTriple.getSubject()) + " " + intToString.get(currentTriple.getPredicate()) + " " + intToString.get(currentTriple.getObject()));
		}

		FileHandler.deleteFile(DictUtility.getCachePath("testSerializedTripleHashSet"));
		FileHandler.deleteFile(DictUtility.getCachePath("testSerializedTripleHashMap2"));
	}
	
	@Test
	public void readAllLinesOnly() throws IOException {
		
		StopWatch readAllOnly = new StopWatch();
		
		String filePath = "fb_sample.nt";
		
		for (int i = 0; i < 5; i++) {
			readAllOnly.start();
			
			// Decorate the File reader with the ability to read UTF-8 encoding characters
		    BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF8")); // 64 secs	
//			BufferedReader br = new BufferedReader(new FileReader(filePath));// 73
		    
		    try {	 
			    while ((br.readLine()) != null) {
			    	// Do nothing
		        }
		    } catch (IOException ex) {
		    	
		    } finally {
		    	br.close();
		    }
		    readAllOnly.stop();
		    System.out.println("Done in " + readAllOnly.elapsedSeconds() + " seconds.\n\n");
		}
	    RuntimeStats.printStats();
	}	
}