package ua.dbproject.indexer;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import ua.dbproject.dictionary.DictUtility;
import ua.dbproject.serializer.ObjectSerializerGZIP;

public class MyBinarySerializerTest {

	private ObjectSerializerGZIP objectSerializer = ObjectSerializerGZIP.getInstance(); 
	
	@SuppressWarnings("resource")
	@Test
	public void testSerializeHashSetGeneric() throws IOException, ClassNotFoundException {
		String filename = "savedHashSet.dat";

		// Create it
		Set<Integer> someStrings = new HashSet<Integer>();
		someStrings.add(33);
		someStrings.add(66);

		// Serialize / save it
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename));
		oos.writeObject(someStrings);

		// Deserialize / load it
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename));
		@SuppressWarnings("unchecked")
		Set<Integer> aNewSet = (HashSet<Integer>) ois.readObject();
		assertNotNull(aNewSet);
	}
	
	@Test
	public void testSerializeHashSet() throws FileNotFoundException, IOException {
		HashSet<Integer> testHashSet = new HashSet<Integer>();
		testHashSet.add(66);
		testHashSet.add(99);
		String pathToSave = Paths.get("").toAbsolutePath().toString() + "/cache/";

		objectSerializer.serializeIntHashSet(testHashSet, pathToSave + "\\" + "testHashSet");
	}
	
	@Test
	public void testDeSerializeHashSet() throws ClassNotFoundException, IOException {
		HashSet<Integer> testHashSet = new HashSet<Integer>();
		for (int i = 0; i < 1000; i++) {
			testHashSet.add(33 + i * 100);
		}

		objectSerializer.serializeIntHashSet(testHashSet, DictUtility.getPathToSave() + "\\" + "testHashSet");		

		HashSet<Integer> testHashSet2 = new HashSet<Integer>();
		testHashSet2 = objectSerializer.deserializeIntHashSet(DictUtility.getPathToSave() + "\\" + "testHashSet");
		assertNotNull(testHashSet2);
	}

}
