package ua.dbproject.dictionary;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ConcurrentNavigableMap;

import org.junit.Test;
import org.mapdb.Atomic;
import org.mapdb.DB;
import org.mapdb.DBMaker;

import ua.dbproject.optimizer.StopWatch;

public class MapDbTest {

	@Test
	public void mapDbWritePersistent() {
		File flDb = new File("dict");
		DB db = DBMaker.newFileDB(flDb).closeOnJvmShutdown().make();

		ConcurrentNavigableMap<Integer, String> map = db.getTreeMap("dict");
		ConcurrentNavigableMap<String, Integer> reverseMap = db.getTreeMap("reverseMap");
		Atomic.Integer keyinc = db.getAtomicInteger("map_keyinc");

		Integer key = -1;
		
		for (int i = 0; i < 5000; i++) {
				
			map.put(key, "AlbertEinstein" + i);
			reverseMap.put("AlbertEinstein" + i, key);
			key = keyinc.incrementAndGet();
		}
		
		System.out.println("Get value for key 2: " + map.get(66));
		System.out.println("Get key for value 'some string': " + reverseMap.get("AlbertEinstein33"));
		
		db.commit();
		db.close();
	}
	
	@Test
	public void mapDbReadPersistent() {
		//DB db = DBMaker.newTempFileDB().make();
		File flDb = new File("dict");
		DB db = DBMaker.newFileDB(flDb).closeOnJvmShutdown().make();

		ConcurrentNavigableMap<Integer, String> map = db.getTreeMap("dict");
		ConcurrentNavigableMap<String, Integer> reverseMap = db.getTreeMap("reverseMap");
		
		System.out.println("Get value for key 2: " + map.get(336));
		System.out.println("Get key for value 'some string': " + reverseMap.get("AlbertEinstein333"));
		
		//db.commit();
		db.close();
	}
	
	@Test
	public void testMapDbDictionary() throws ClassNotFoundException, IOException {
		MapDbDictionary mapDbDictionary = MapDbDictionary.getInstance();
		System.out.println("k: " + mapDbDictionary.getInt("Daddy's Gone A-Hunting@en"));
		System.out.println("l: " + mapDbDictionary.getInt("l"));
		StopWatch timer1 = new StopWatch();
		timer1.start();
		for (int i = 0; i < 50; i++) {
			System.out.println("String" + i + ": " + mapDbDictionary.getString(i));
		}
		timer1.stop();
		System.out.println("count: " + mapDbDictionary.getCount());
		System.out.println("Elapsed: " + timer1.elapsed());
	}
}
