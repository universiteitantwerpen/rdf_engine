package ua.dbproject.parsers;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ua.dbproject.db.DbController;
import ua.dbproject.dictionary.DbDictionary;
import ua.dbproject.dictionary.DictUtility;
import ua.dbproject.dictionary.GenericDictionary;
import ua.dbproject.indexer.IndexManager;

/**
 * The TTL_Parser takes care of parsing TTL files that hold the RDF input data.
 */
public class InputParser {
	
	/**
	 * Instance of the HashDictionary for String to Integer conversions (Singleton object)
	 */
	private static DbDictionary dbDictionary = DbDictionary.getInstance();	
	private static IndexManager indexManager = IndexManager.getInstance();
	
	private static int countMatches = 0;
	
	/**
	 * Reads from a ttl and stores to main memory and the database
	 * 
	 * @param filePath
	 * @param db
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws IOException 
	 */
	public static boolean readInputStoreToDb(String filePath, DbController db) throws ClassNotFoundException, SQLException, IOException {
		
		FileHandler.createFolder(DictUtility.getPathToSave());
		
		if(FileHandler.fileExists(filePath)) {
			
			// Decorate the File reader with the ability to read UTF-8 encoding characters
		    BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF8"));
		    
		    try {	 
			    String line = null;
		        while ((line = br.readLine()) != null) {
		        	processStoreNTTriples(filePath, db, line);
		        }
		        
		        // if there are any extra values left
		        if (indexManager.getMainDictCount() > 0)
		        	storeTriplesToDb(db);
		        
		        // store last dictionary
		        dbDictionary.toPersistentStorage(db);
		        
		    } catch (IOException e) {
				e.printStackTrace();
				return false;
			} finally {
		        try {
		        	dbDictionary.close();
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
		    }
		    return true;
		}
		else
			return false;
	}

	@SuppressWarnings("unused")
	private static void processStoreTTLTriples(String filePath, DbController db, String line) throws SQLException, ClassNotFoundException,
			IOException {
		String[] temp = line.split("\t");
		
		if(temp.length > 0) {	            	
		
			if (!DictUtility.isTTLPrefix(temp[0]) && (temp.length >= 3)) { // if matches # or @, continue
				indexManager.add(DictUtility.removeXsd(DictUtility.removeEqualityChars(temp[0])), DictUtility.removeEqualityChars(temp[1]),
						DictUtility.removeXsd(DictUtility.removeDot(DictUtility.removeEqualityChars(temp[2]))));
				
		        storeTriples(filePath, db);
			}	                		  
		}
	}
	
	public static boolean readInputStoreToDb(String filePath, DbController db, int count) throws ClassNotFoundException, SQLException, IOException {

		FileHandler.createFolder(DictUtility.getPathToSave());
		
		if(FileHandler.fileExists(filePath)) {
			
			// Decorate the File reader with the ability to read UTF-8 encoding characters
		    BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF8"));
		    
		    try {	 
			    String line = null;
	        	Messenger.show("Started readling lines...");
	        	
	        	int lineCounter = 0;
			    countMatches = 0;
		        while ((line = br.readLine()) != null) {
		        	processStoreNTTriples(filePath, db, line, count);
		        	lineCounter++;
		        }
		        Messenger.show("Read lines: " + lineCounter);
		        Messenger.show("Matches found: " + countMatches);
		        
		        // if there are any extra values left
		        if (indexManager.getMainDictCount() > 0) {
					Messenger.show("Last values read: " + indexManager.getCount());
		        	storeTriplesToDb(db);
		        }
		        // store last dictionary
		        dbDictionary.toPersistentStorage(db);
		        
		    } catch (IOException e) {
				e.printStackTrace();
				return false;
			} finally {
		        try {
		        	dbDictionary.close();
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
		    }
		    return true;
		}
		else
			return false;
	}
	
	private static void processStoreNTTriples(String filePath, DbController db, String line, int count) throws SQLException, ClassNotFoundException,
	IOException {
		
		String strPattern = "";
		
		strPattern += "((\".*\"\\^\\^.*>)|<(.*)>|\"(.*)\"|(\".*\"\\@[A-Za-z][A-Za-z].*))";		
		for (int i = 1; i < count; i++) {
			strPattern += " ((\".*\"\\^\\^.*>)|<(.*)>|\"(.*)\"|(\".*\"\\@[A-Za-z][A-Za-z].*))";
		}
		
		Pattern pattern = Pattern.compile(strPattern);
		
		Matcher matcher = pattern.matcher(line);
	    
	    if (matcher.matches()) {
		    countMatches++;
	    	switch (count) {
		    	case 17:
	    			indexManager.add(matcher.group(1), matcher.group(6), matcher.group(11));
	    			indexManager.add(matcher.group(16), matcher.group(21), matcher.group(26));
	    			indexManager.add(matcher.group(31), matcher.group(36), matcher.group(41));
	    			indexManager.add(matcher.group(46), matcher.group(51), matcher.group(56));
	    			indexManager.add(matcher.group(61), matcher.group(66), matcher.group(71));
	    			indexManager.add(matcher.group(76), matcher.group(81), matcher.group(1));
	    			break;
		    	case 10:
	    			indexManager.add(matcher.group(1), matcher.group(6), matcher.group(11));
	    			indexManager.add(matcher.group(16), matcher.group(21), matcher.group(26));
	    			indexManager.add(matcher.group(31), matcher.group(36), matcher.group(41));
	    			indexManager.add(matcher.group(46), matcher.group(46), matcher.group(46));
	    			break;
	    		case 9:
	    			indexManager.add(matcher.group(1), matcher.group(6), matcher.group(11));
	    			indexManager.add(matcher.group(16), matcher.group(21), matcher.group(26));
	    			indexManager.add(matcher.group(31), matcher.group(36), matcher.group(41));
	    			break;
	    		case 8:
	    			indexManager.add(matcher.group(1), matcher.group(6), matcher.group(11));
	    			indexManager.add(matcher.group(16), matcher.group(21), matcher.group(26));
	    			indexManager.add(matcher.group(31), matcher.group(36), matcher.group(31));
	    			break;
	    		case 7:
	    			indexManager.add(matcher.group(1), matcher.group(6), matcher.group(11));
	    			indexManager.add(matcher.group(16), matcher.group(21), matcher.group(26));
	    			indexManager.add(matcher.group(31), matcher.group(31), matcher.group(31));
	    			break;
	    		case 6:
	    			indexManager.add(matcher.group(1), matcher.group(6), matcher.group(11));
	    			indexManager.add(matcher.group(16), matcher.group(21), matcher.group(26));
	    			break;
	    		case 5:
	    			indexManager.add(matcher.group(1), matcher.group(6), matcher.group(11));
	    			indexManager.add(matcher.group(16), matcher.group(21), matcher.group(21));
	    			break;
	    		case 4:
	    			indexManager.add(matcher.group(16), matcher.group(16), matcher.group(16));//3
	    		case 3:
	    			indexManager.add(matcher.group(1), matcher.group(6), matcher.group(11));//3
	    			break;
	    		case 2:
	    			indexManager.add(matcher.group(1), matcher.group(6), matcher.group(6));//3
	    			break;
	    		case 1:
	    			indexManager.add(matcher.group(1), matcher.group(1), matcher.group(1));//3
	    			break;
	    		}
	    	} else {
	    		Messenger.show("No match for: " + line);
	    	
	        storeTriples(filePath, db);
	    }
	}
	
	private static void processStoreNTTriples(String filePath, DbController db, String line) throws SQLException, ClassNotFoundException,
	IOException {

		Pattern pattern = Pattern.compile("((\".*\"\\^\\^.*>)|<(.*)>|_:(.*)[1-9]|\"(.*)\" |(\".*\"\\@[A-Za-z\\-0-9]* .*)).*"
				+ "((\".*\"\\^\\^.*>)|<(.*)>|_:(.*)[1-9]|\"(.*)\" |(\".*\"\\@[A-Za-z\\-0-9]* .*)).*"
				+ "((\".*\"\\^\\^.*>)|<(.*)>|_:(.*)[1-9]|\"(.*)\" |(\".*\"\\@[A-Za-z\\-0-9]* .*)).*\\.");
		
		Matcher matcher = pattern.matcher(line);
	    
	    if (matcher.matches()) {
	    	indexManager.add(matcher.group(1), matcher.group(7), matcher.group(13));
	    	
	        storeTriples(filePath, db);
	    } else {
    		Messenger.show("No match for: " + line);
	    }
	}

	private static void storeTriples(String filePath, DbController db) throws SQLException, ClassNotFoundException, IOException {
		if (countModLimit(indexManager.getCount(), GenericDictionary.MessageEvery))
			Messenger.show("Triples read: " + indexManager.getCount());
		
		if (countModLimit(indexManager.getCount(), GenericDictionary.RDFChunkSize)) {
			storeTriplesToDb(db);
	        dbDictionary.toPersistentStorage(db);
		}
	}
	
	/**
	 * Check if the count has superseded the size limit
	 * 
	 * @param count
	 * @param size
	 * @return
	 */
	private static boolean countModLimit(long count, int size) {
		if(count >= size && count % size == 0)
			return true;
		else
			return false;
	}
	
	/**
	 * Store the buffered dictionary to disk
	 * 
	 * @param filePath
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static void storeDictionaryToDisk(String filePath) throws ClassNotFoundException, IOException {
		Messenger.show("Saving dictionary to disk...");
		dbDictionary.toDisk("");
	}	

	/**
	 * Store the buffered triples to the database
	 * 
	 * @param db
	 * @throws SQLException
	 */
	public static void storeTriplesToDb(DbController db) throws SQLException {
		Messenger.show("Saving triples to db...");	
		db.triplesToDb();
	}
}
