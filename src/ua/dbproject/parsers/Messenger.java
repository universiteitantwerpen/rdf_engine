package ua.dbproject.parsers;

/**
 * Utility Class to show messages on the console output
 * @version 0.1
 */
public class Messenger {

	private static int previousCharacterCount = 0;
	
	private static boolean debugEnabled = true;
	
	/**
	 * Print the string message to a new line
	 * 
	 * @param msg The message to be displayed to the console
	 */
	public static void show(String msg) {
		if (debugEnabled) {
			System.out.println(msg);
			previousCharacterCount = msg.length();
		}
	}
	
	public static void setDebug(boolean debugMode) {
		debugEnabled = debugMode;
	}

	/**
	 * Print the string message to the same line, by deleting the previous line
	 * 
	 * @param msg The message to be displayed to the console
	 */
	public static void showSameLine(String msg) {
		if (debugEnabled) {
			System.out.print(msg);
			previousCharacterCount = msg.length();
		}
	}
}
