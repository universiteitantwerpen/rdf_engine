package ua.dbproject.parsers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Static class that reads TTL files and indexes them to the main tripleHashMap dictionary
 * 
 * @author madks_000
 * @version 0.1
 *
 */
public class FileHandler {
	
	/** 
	 * Locate and delete a file using the local file system
	 */
	public static void deleteFile(String fileName) {   	 
		File file = new File(fileName);
		file.delete(); 
	}
	
	/**
	 * Check if file exists on disk
	 */
	public static boolean fileExists(String fileName) {
		
		File f = new File(fileName);
		if(f.exists())
			return true;
		else
			return false;
	}
	
	/**
	 * Generates folder on disk.<P>If the folder already exists it skips the mkdir command
	 * 
	 * @param fileName The folder path
	 */
	public static void createFolder(String fileName) {
		
		File f = new File(fileName);
		if(!f.exists())
			f.mkdirs();
	}
	
	/** 
	 * Locate the file in the "filePath" location and return its value as a String.
	 * <p>Remarks: Reads one line at a time 
	 * 
	 * @param fileName The folder path
	 */
	public static String readFile(String filePath) throws FileNotFoundException {
	    BufferedReader br = new BufferedReader(new FileReader(filePath));
	    StringBuilder sb = null;
	    String everything = null;
	    try {
	        sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            line = br.readLine();
	        }
	        everything = sb.toString();
	    } catch (IOException e) {
			e.printStackTrace();
		} finally {
	        try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	    }
	    return everything;
	}
	
	/** 
	 * Write to disc the String "source" as "fileName" in the current executable's path location 
	 * 
	 * @param source The string to be written to disk
	 * @param fileName The name of the output file
	 */
	public static void writeFile(String source, String fileName) {
		BufferedWriter writer = null;
	    try {
	    	//create a temporary file
	    	File logFile = new File(fileName);
		
		    writer = new BufferedWriter(new FileWriter(logFile, true));
		    writer.write(source);
		} catch (Exception e) {
		    e.printStackTrace();
		} finally {
		    try {
		        // Close the writer regardless of what happens...
		        writer.close();
		    } catch (Exception e) {
		    }
		}
	}
	
	public static void writeBytes(String source, String fileName, long lines) throws IOException {
		@SuppressWarnings("unused")
		BufferedWriter writer = null;
		byte[] buffer = source.getBytes();
		@SuppressWarnings("resource")
		FileChannel rwChannel = new RandomAccessFile(fileName, "rw").getChannel();
		ByteBuffer wrBuf = rwChannel.map(FileChannel.MapMode.READ_WRITE, 0, buffer.length * lines);
		for (int i = 0; i < lines; i++){
		    wrBuf.put(buffer);
		}
		rwChannel.close();
	}
	
	public static void writeBytes(StringBuilder source, String fileName, long lines) throws IOException {
		writeBytes(source.toString(), fileName, lines);
	}
	
	public static void writeFile(StringBuilder source, String fileName) {
		writeFile(source.toString(), fileName);
	}
}
