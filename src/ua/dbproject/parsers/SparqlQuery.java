package ua.dbproject.parsers;

import java.util.ArrayList;

import ua.dbproject.dictionary.JoinItem;
import ua.dbproject.indexer.Triple;

public final class SparqlQuery {
	private ArrayList<Triple<RdfItem, RdfItem, RdfItem>> joins = new ArrayList<Triple<RdfItem, RdfItem, RdfItem>> ();
	private ArrayList<RdfItem> variables = new ArrayList<RdfItem> ();
	private ArrayList<Triple<RdfItem, String, String>> attachedVariables = new ArrayList<Triple<RdfItem, String, String>> ();
	private ArrayList<Triple<String, JoinItem, JoinItem>> tableCorrelations = new ArrayList<Triple<String, JoinItem, JoinItem>> ();
	
	private boolean distinct = false;
	
	public SparqlQuery(ArrayList<RdfItem> variables, ArrayList<Triple<RdfItem, RdfItem, RdfItem>> joins) {
		this(variables, joins, false);
	}
	
	public int getColumnCount() {
		return variables.size();
	}
	
	public SparqlQuery(ArrayList<RdfItem> variables, ArrayList<Triple<RdfItem, RdfItem, RdfItem>> joins, boolean distinct) {
		this.variables = variables;		
		this.joins = joins;
		this.distinct = distinct;
		
		if (this.variables.size() == 1 && this.variables.get(0).getValue().equals("*"))
			this.variables = generateVariables(this.joins);
	}
	
	private boolean isExistingVariable(ArrayList<RdfItem> varList, String variableName) {
		
		for (RdfItem currentItem : varList) {
			if (currentItem.getValue().equals(variableName))
				return true;
		}
		
		return false;
	}
	
	private ArrayList<RdfItem> generateVariables(ArrayList<Triple<RdfItem, RdfItem, RdfItem>> joinsList) {
		ArrayList<RdfItem> returningVariables = new ArrayList<RdfItem> ();
		
		for (Triple<RdfItem, RdfItem, RdfItem> currentJoin : joinsList) {
			if (currentJoin.getSubject().getType() == RdfItemType.VARIABLE)
				if (!isExistingVariable(returningVariables, currentJoin.getSubject().getValue()))
					returningVariables.add(currentJoin.getSubject());
			if (currentJoin.getPredicate().getType() == RdfItemType.VARIABLE)
				if (!isExistingVariable(returningVariables, currentJoin.getPredicate().getValue()))
					returningVariables.add(currentJoin.getPredicate());
			if (currentJoin.getObject().getType() == RdfItemType.VARIABLE)
				if (!isExistingVariable(returningVariables, currentJoin.getObject().getValue()))
					returningVariables.add(currentJoin.getObject());
		}
		
		return returningVariables;
	}
	
	private String getCorrelation(int index) {
		String returningString = "";
		
		if (index > 0) {
			returningString += " ON ";
			
			Triple<String, JoinItem, JoinItem> currentCorrelation = tableCorrelations.get(index);
			
			returningString += currentCorrelation.getPredicate().getTable() + "." + currentCorrelation.getPredicate().getType() +
					" = " + currentCorrelation.getObject().getTable() + "." + currentCorrelation.getObject().getType();
		}
		return returningString;
	}
		
	private void generateCorrelations() {
		int currentCounter = 0;
		for (Triple<RdfItem, RdfItem, RdfItem> currentJoin : joins) {
			
			int otherCounter = 0;
			for (Triple<RdfItem, RdfItem, RdfItem> otherJoin : joins) {
				
				if (currentCounter != otherCounter) {								
					if (currentJoin.getSubject().getValue().equals(otherJoin.getSubject().getValue())) {
						tableCorrelations.add(new Triple<String, JoinItem, JoinItem> ("t" + currentCounter,
								new JoinItem("t" + currentCounter, "subject"), new JoinItem("t" + otherCounter, "subject")));
						break;
					}
					if (currentJoin.getSubject().getValue().equals(otherJoin.getPredicate().getValue())) {
						tableCorrelations.add(new Triple<String, JoinItem, JoinItem> ("t" + currentCounter,
								new JoinItem("t" + currentCounter, "subject"), new JoinItem("t" + otherCounter, "predicate")));
						break;
					}
					if (currentJoin.getSubject().getValue().equals(otherJoin.getObject().getValue())) {
						tableCorrelations.add(new Triple<String, JoinItem, JoinItem> ("t" + currentCounter,
								new JoinItem("t" + currentCounter, "subject"), new JoinItem("t" + otherCounter, "object")));
						break;
					}
					if (currentJoin.getPredicate().getValue().equals(otherJoin.getSubject().getValue())) {
						tableCorrelations.add(new Triple<String, JoinItem, JoinItem> ("t" + currentCounter,
								new JoinItem("t" + currentCounter, "predicate"), new JoinItem("t" + otherCounter, "subject")));
						break;
					}
					if (currentJoin.getPredicate().getValue().equals(otherJoin.getPredicate().getValue())) {
						tableCorrelations.add(new Triple<String, JoinItem, JoinItem> ("t" + currentCounter,
								new JoinItem("t" + currentCounter, "predicate"), new JoinItem("t" + otherCounter, "predicate")));
						break;
					}
					if (currentJoin.getPredicate().getValue().equals(otherJoin.getObject().getValue())) {
						tableCorrelations.add(new Triple<String, JoinItem, JoinItem> ("t" + currentCounter,
								new JoinItem("t" + currentCounter, "predicate"), new JoinItem("t" + otherCounter, "object")));
						break;
					}
					if (currentJoin.getObject().getValue().equals(otherJoin.getSubject().getValue())) {
						tableCorrelations.add(new Triple<String, JoinItem, JoinItem> ("t" + currentCounter,
								new JoinItem("t" + currentCounter, "object"), new JoinItem("t" + otherCounter, "subject")));
						break;
					}
					if (currentJoin.getObject().getValue().equals(otherJoin.getPredicate().getValue())) {
						tableCorrelations.add(new Triple<String, JoinItem, JoinItem> ("t" + currentCounter,
								new JoinItem("t" + currentCounter, "object"), new JoinItem("t" + otherCounter, "predicate")));
						break;
					}
					if (currentJoin.getObject().getValue().equals(otherJoin.getObject().getValue())) {
						tableCorrelations.add(new Triple<String, JoinItem, JoinItem> ("t" + currentCounter,
								new JoinItem("t" + currentCounter, "object"), new JoinItem("t" + otherCounter, "object")));
						break;
					}
				}
				
				otherCounter++;
			}
			currentCounter++;
		}
	}
	
	private void attachVariables() {
		for (RdfItem currentVariable : variables) {
			
			String currentValue = currentVariable.getValue();
			// find the var in the joins arraylist
			int tripleCounter = 0;
			String tableName = "";
			String itemType = "";
			
			
			for (Triple<RdfItem, RdfItem, RdfItem> currentTriple : joins ) {
				
				if (currentValue.equals(currentTriple.getSubject().getValue())) {					
					tableName = "t" + tripleCounter;	// get the table name by the counter	
					itemType = "subject";	// get the predicate/subject/object part
					break;
				}
				if (currentValue.equals(currentTriple.getPredicate().getValue())) {						
					tableName = "t" + tripleCounter;	// get the table name by the counter	
					itemType = "predicate";	// get the predicate/subject/object part
					break;
				}
				if (currentValue.equals(currentTriple.getObject().getValue())) {						
					tableName = "t" + tripleCounter;	// get the table name by the counter	
					itemType = "object";	// get the predicate/subject/object part
					break;
				}				
				
				tripleCounter++;
			}
			// add the triple
			attachedVariables.add(new Triple<RdfItem, String, String> (currentVariable, tableName, itemType));
		}
	}
	
	public String toSQL() {
		String returningString = "";
		
		returningString = "SELECT ";
		
		if (this.distinct)
			returningString += "DISTINCT ";
		
		attachVariables();
		
		int varCounter = 0;
		String varsString = "";
		for (Triple<RdfItem, String, String> currentVariable : attachedVariables) {
			varsString = currentVariable.getPredicate() + "." + currentVariable.getObject()
					+ " AS " + currentVariable.getSubject().getValue(); 
			
			if (varCounter > 0)
				varsString = ", " + varsString; 

			returningString += varsString;
			varCounter++;
		}
		
		generateCorrelations();
		
		int tripleCounter = 0;
		int whereCounter = 0;
		String strWhere = "";		
		for (Triple<RdfItem, RdfItem, RdfItem> currentTriple : joins) {
			String strTable = "t" + tripleCounter;			
			String strRow = "";
			
			int constantCounter = 0; 
			
			if (currentTriple.getSubject().getType() == RdfItemType.CONSTANT) {
				strWhere += checkConstantCounter(whereCounter++);
				constantCounter++;
				strWhere += strTable + ".subject=" + currentTriple.getSubject().getIntValue();
			}
			
			if (currentTriple.getPredicate().getType() == RdfItemType.CONSTANT) {
				strWhere += checkConstantCounter(whereCounter++);
				constantCounter++;
				strWhere += strTable + ".predicate=" + currentTriple.getPredicate().getIntValue();
			}
			
			if (currentTriple.getObject().getType() == RdfItemType.CONSTANT) {
				strWhere += checkConstantCounter(whereCounter++);
				constantCounter++;
				strWhere += strTable + ".object=" + currentTriple.getObject().getIntValue();
			}
			
			if (constantCounter > 0) {
				strRow = checkTripleCounter(tripleCounter) + strTable + getCorrelation(tripleCounter) + strRow;
				returningString += strRow;
			}
			tripleCounter++;
		}		
		return returningString + strWhere + ";";
	}
	
	private String checkTripleCounter(int counter) {
		if (counter == 0) {
			return " FROM tripleSPO AS ";
		} else {
			return " INNER JOIN tripleSPO AS ";
		}
	}
	
	private String checkConstantCounter(int counter) {
		if (counter == 0)
			return " WHERE ";
		else
			return " AND ";	
	}
}
