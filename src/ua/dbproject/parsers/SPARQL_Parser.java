package ua.dbproject.parsers;

import java.util.ArrayList;
import java.util.Arrays;

import ua.dbproject.indexer.Triple;

/** The SPARQL_Parser takes care of parsing SPARQL queries. */
public class SPARQL_Parser {	
	public ArrayList<String> getVariables(String query) {
		
		String currentQuery = stripDistinct(query);
		
		String[] returningString = currentQuery.split("WHERE");		
		String querySelectPart = returningString[0];
		
		querySelectPart = querySelectPart.replace(" ", "");
		querySelectPart = querySelectPart.replace("SELECT", "");
		String[] vars = querySelectPart.split("\\?");

		ArrayList<String> returningListStripped =  new ArrayList<String>(Arrays.asList(vars));
		returningListStripped.removeAll(Arrays.asList(null,""));
		
		ArrayList<String> returningList = new ArrayList<String>();
		
		for (String item : returningListStripped)
			returningList.add("?" + item.replace(",", ""));
		
		return returningList;
	}
	
	private String stripDistinct(String query) {
		String returningString = query;
		
		if (isDistinct(query)) {
			returningString = query.replace("DISTINCT", "");
		}
		
		return returningString;
	}
	
	public ArrayList<Triple<String, String, String>> getJoins(String query) {
		ArrayList<Triple<String, String, String>> returningList =  new ArrayList<Triple<String, String, String>>();
		int startingIndex = 0;
		
		String[] returningString = query.split("WHERE");	
		
		String tempQueryJoinsPart = returningString[1];
		tempQueryJoinsPart = tempQueryJoinsPart.replace("{", "").replace("}", "");
		
		String[] joins = tempQueryJoinsPart.split(" \\.");
		
		if (joins.length == 0) {
			String[] joinMembers = tempQueryJoinsPart.split("\\s+");
			
			startingIndex = getNotNullIndex(joinMembers);
			
			String strSubject = joinMembers[startingIndex];
			String strPredicate = joinMembers[startingIndex + 1];
			String strObject = joinMembers[startingIndex + 2];
			
			returningList.add(new Triple<String, String, String> (strSubject, strPredicate, strObject));
			
		} else {
			
			for (String joinTriple : joins) {
				String[] joinMembers = joinTriple.split("\\s+");
				
				startingIndex = getNotNullIndex(joins);
				
				ArrayList<String> list = new ArrayList<String>();
				
			    for(String s : joinMembers)
			        if(s != null && s.length() > 0)
			           list.add(s);
				
				returningList.add(new Triple<String, String, String> (list.get(0), list.get(1), list.get(2)));
			}
		}
		returningList.removeAll(Arrays.asList(null,""));
		
		return returningList;		
	}
	
	public ArrayList<Triple<RdfItem, RdfItem, RdfItem>> getRdfJoins(String query) {
		ArrayList<Triple<RdfItem, RdfItem, RdfItem>> listRdfItems = new ArrayList<Triple<RdfItem, RdfItem, RdfItem>> ();
		
		ArrayList<Triple<String, String, String>> listStrings = new ArrayList<Triple<String, String, String>> ();
		listStrings = this.getJoins(query);
		
		for (Triple<String, String, String> strTriple : listStrings) {
			listRdfItems.add(new Triple<RdfItem, RdfItem, RdfItem>(new RdfItem(strTriple.getSubject()), 
					new RdfItem(strTriple.getPredicate()), new RdfItem(strTriple.getObject())));
		}
		
		return listRdfItems;
	}
	
	public boolean isDistinct(String query) {
		if (query.matches(".*(DISTINCT).*"))
			return true;
		else
			return false;
	}
	
	public ArrayList<RdfItem> getRdfVariables(String query) {
		ArrayList<RdfItem> listRdfItems = new ArrayList<RdfItem> ();
		
		ArrayList<String> listStringVars = this.getVariables(query);
		
		for (String strItem : listStringVars) {
			listRdfItems.add(new RdfItem(strItem));
		}
		
		return listRdfItems;
	}
	
	private int getNotNullIndex(String[] inputStringTable) {
		int startingIndex = 0;
		if (inputStringTable[startingIndex] == null)
			startingIndex = 1;
		else
			startingIndex = 0;
		
		return startingIndex;
	}
}
