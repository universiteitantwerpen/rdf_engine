package ua.dbproject.parsers;

import ua.dbproject.dictionary.MapDbDictionary;

public final class RdfItem {	

	private MapDbDictionary mapDbDictionary = MapDbDictionary.getInstance();

	private String rawValue = null;
	private String value = null;
	private String intValue = null;
	
	private RdfItemType type = RdfItemType.GENERIC;
	
	public RdfItem(String strValue) {
		this.rawValue = strValue;
		this.type = getType(strValue);
		this.value = stripRawValue(strValue);
		this.intValue = Integer.toString(mapDbDictionary.addString(this.value));
	}
	
	private RdfItemType getType(String strValue) {
		RdfItemType typeToReturn = RdfItemType.GENERIC;
		
		if (strValue.matches(".*(\\?).*"))
			typeToReturn = RdfItemType.VARIABLE;
		else
			typeToReturn = RdfItemType.CONSTANT;
		
		return typeToReturn;
	}
	
	private String stripRawValue(String rawValue) {
		String strToReturn = rawValue;
		
		if (this.type == RdfItemType.VARIABLE)
			strToReturn = rawValue.replace("?", "");
		
		return strToReturn;
	}
	
	public String getRawValue() {
		return this.rawValue;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public String getIntValue() {
		return this.intValue;
	}
	
	public RdfItemType getType() {
		return this.type;
	}
}
