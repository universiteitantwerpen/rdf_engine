package ua.dbproject.parsers;

/**
 * The tree different RdfItem object types for the Sparql Parsing
 * 
 * @author ktheodorakos
 *
 */
public enum RdfItemType {
	CONSTANT, VARIABLE, GENERIC
}
