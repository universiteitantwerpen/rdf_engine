package ua.dbproject.db;

/**
 * Data class that helps in using custom database settings
 */
public class DbParameters {
	private String connectionInfo = null;
	private String databaseSchemaName = null;
	private String databaseUserName = null;
	private String databasePassword = null;
	private String JDBCDriver = null;	
	private String port = null;
	private String ip = null;
	
	private String serverEngine = null;

	/**
	 * Constructor that sets the database settings 
	 * 
	 * @param jDBCDriver The name of the JDBC driver, ie org.postgresql.Driver
	 * @param serverEngine Name of the database engine, i.e. monetdb or postgresql
	 * @param ip The ip address of the database server
	 * @param port The port for the connection with the database
	 * @param databaseSchemaName The name of the database schema 
	 * @param databaseUserName The username for the connection with the database
	 * @param databasePassword The password for the connection with the database
	 */
	public DbParameters(String jDBCDriver, String serverEngine, String ip, String port, String databaseSchemaName, String databaseUserName, String databasePassword) {		
		this.setDatabaseSchemaName(databaseSchemaName);
		this.setDatabaseUserName(databaseUserName);
		this.setDatabasePassword(databasePassword);
		this.JDBCDriver = jDBCDriver;
		this.serverEngine = serverEngine;
		this.setPort(port);
		this.setIp(ip);
		this.setConnectionInfo(String.format("jdbc:%s://%s:%s/%s", serverEngine, ip, port, databaseSchemaName));
	}
	
	/**
	 * Check if the JDBC driver exists
	 * @return Returns true if it exists
	 */
	public boolean isSetJDBCDriver() {
		if (this.JDBCDriver == null)
			return false;
		else
			return true;
	}
	
	/**
	 * Set the name of the currently used database engine
	 * 
	 */
	public void setDbEngine(String serverEngine) {
		this.serverEngine = serverEngine;
	}
	
	/**
	 * Get the name of the currently used database engine
	 * @return String with the name of the database (contained within the connection string)
	 */
	public String getDbEngine() {
		return this.serverEngine;
	}
	
	public String getJDBCDriver() {
		return JDBCDriver;
	}

	public String getDatabaseSchemaName() {
		return databaseSchemaName;
	}

	public void setDatabaseSchemaName(String databaseSchemaName) {
		this.databaseSchemaName = databaseSchemaName;
	}

	public String getConnectionInfo() {
		return connectionInfo;
	}

	public void setConnectionInfo(String connectionInfo) {
		this.connectionInfo = connectionInfo;
	}

	public String getDatabasePassword() {
		return databasePassword;
	}

	public void setDatabasePassword(String databasePassword) {
		this.databasePassword = databasePassword;
	}

	public String getDatabaseUserName() {
		return databaseUserName;
	}

	public void setDatabaseUserName(String databaseUserName) {
		this.databaseUserName = databaseUserName;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
}