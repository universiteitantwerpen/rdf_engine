package ua.dbproject.db;

/**
 * Helps in choosing a database within the DbController constructor
 */
public enum DatabaseEngine {
	MONETDB, POSTGRESQL
}