package ua.dbproject.db;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;

import ua.dbproject.indexer.IndexManager;
import ua.dbproject.indexer.Triple;
import ua.dbproject.optimizer.StopWatch;
import ua.dbproject.parsers.Messenger;
import ua.dbproject.statistics.SettingsReader;

/**
 * Class that connects/closes connection with the db, executes DML, DDL and DSL operations 
 */
public class DbController {

	/**
	 * Connection objects that holds the current connection
	 */
	private Connection connection = null;
	
	/**
	 * DbParameters is a settings container class
	 */
	private DbParameters dbParameters = new DbParameters(null, null, null, null, null, null, null);
	
	/**
	 * If debug is disabled, then no debugging/informational messages are being displayed
	 */
	private boolean isDebugEnabled = false;
	
	/**
	 * Stores the current statement
	 */
	private Statement stmt = null;
	
	/**
	 * Singleton instance of the manager
	 */
	private IndexManager indexManager = IndexManager.getInstance();
	
	/*
	 * Stores the names of the tables that will be added in the database
	 */
	private ArrayList<String> tables = new ArrayList<String>();
	
	/**
	 * Default constructor, reads settings from xml file
	 */
	public DbController() {
	
		SettingsReader xmlSettingsReader = new SettingsReader();		
		dbParameters = xmlSettingsReader.getXmlSettings();
		isDebugEnabled = xmlSettingsReader.isDebugEnabled();
	}
	
	/**
	 * Check is debugging is set to enable on the settings.xml
	 * 
	 * @return
	 */
	public boolean isDebugEnabled() {
		return this.isDebugEnabled;
	}
	
	/**
	 * Constructor that enables to choose between the pre configured database engines
	 * @param dbEngineEnum
	 * @throws ClassNotFoundException 
	 */
	public DbController(DatabaseEngine dbEngineEnum) {		
		switch(dbEngineEnum) {
		case POSTGRESQL:
			/**
			 * PostgreSQL connector
			 */
			dbParameters = new DbParameters("org.postgresql.Driver", "postgresql", "127.0.0.1", "5432", "dbProject", "madks", "ma121284");
			break;
		case MONETDB:
			/**
			 * MonetDB connector
			 */
			dbParameters = new DbParameters("nl.cwi.monetdb.jdbc.MonetDriver", "monetdb", "127.0.0.1", "50000", "dbProject", "monetdb", "monetdb");
			break;
		}
	}
	
	/**
	 * Check if the JDBC driver exists
	 * @return Returns true if it exists
	 * @throws ClassNotFoundException 
	 */
	private boolean getJDBCDriver() {
		return dbParameters.isSetJDBCDriver();
	}
	
	/** 
	 * Do connect with the locally set db instance/engine
	 * @return Returns true if connection is successful
	 */
	public boolean connect() {

		if (getJDBCDriver()) {
			try {
			    Class.forName(dbParameters.getJDBCDriver());
				connection = DriverManager.getConnection(dbParameters.getConnectionInfo(), dbParameters.getDatabaseUserName(), dbParameters.getDatabasePassword());
			} catch (SQLException | ClassNotFoundException e) {
				e.printStackTrace();
				return false;
			}
			if(connection != null)				
				return true;
			else
				return false;
		}
		return true;
	}
	
	/**
	 * Execute a SQL update-type command (DML), with no expected result set i.e INSERT, UPDATE, etc	
	 * @param query The insert query
	 * @return Returns false on SQLException
	 */
	public boolean executeUpdate(String query) {
		try {
			if (connection != null) {
				stmt = connection.createStatement();
				stmt.executeUpdate(query);
				stmt.close();
			} 
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;		
	}
	
	/**
	 * Get the name of the currently used database engine
	 * @return String with the name of the database (contained within the connection string)
	 */
	public String getDbEngine() {
		return this.dbParameters.getDbEngine();
	}

	/**
	 * Acquire the returning Result set of the current database from the desired SQL query
	 * 
	 * @param query
	 * @param columnCount
	 * @return
	 */
	public ArrayList<String[]> getResultsArrayList(String query, int columnCount) {
		ArrayList <String[]> result = new ArrayList<String[]>();
	
		try {
			if (connection != null) {
				stmt = connection.createStatement();
				
				StopWatch sqlStopWatch = new StopWatch();
				sqlStopWatch.start();
				Messenger.show("Executing query...");
				
				ResultSet rs = stmt.executeQuery(query);
				sqlStopWatch.stop();
				Messenger.show("Received result set in " + sqlStopWatch.elapsed() + "ms");
				
				Messenger.show("Column count: " + columnCount);
				
				Messenger.show("Storing result set to an array list...");
				while(rs.next()) {
				    String[] row = new String[columnCount];
				    for (int i = 0; i < columnCount; i++)
				       row[i] = rs.getString(i + 1);
				    
				    result.add(row);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * Close the connection with the database instance
	 */
	public void close() throws SQLException {
		connection.close();
	}
	
	/**
	 * Do INSERT the current index manager's read Triples into the database schema
	 * 
	 * @throws SQLException
	 */
	public void triplesToDb() throws SQLException {
		if (connection != null) {
			
			PreparedStatement preparedStmt = connection.prepareStatement("INSERT INTO tripleSPO VALUES (?,?,?);");	
			for (Triple<Integer, Integer, Integer> currentTriple : indexManager.getMainDict()) {
				preparedStmt.setInt(1, currentTriple.getSubject());
				preparedStmt.setInt(2, currentTriple.getPredicate());
				preparedStmt.setInt(3, currentTriple.getObject());
				preparedStmt.addBatch();				
			}
			
			preparedStmt.executeBatch();			
			preparedStmt.clearBatch();
			preparedStmt.close();			
			indexManager.flushMainDict();
			System.gc();
		}         	        
    }
	
	/**
	 * Do INSERT the current index manager's read dictionary value into the database schema
	 * 
	 * @param dictList
	 * @throws SQLException
	 */
	public void dictionaryToDb(Map<Integer, String> dictList) throws SQLException {
		 
		if (connection != null) {
			PreparedStatement preparedStmt = connection.prepareStatement("INSERT INTO dictionary VALUES (?,?);");
			
			for (Map.Entry<Integer, String> entry : dictList.entrySet()) {
				preparedStmt.setInt(1, entry.getKey());
				preparedStmt.setString(2, entry.getValue().replaceAll("'", ""));
				preparedStmt.addBatch();
			}	
			
			preparedStmt.executeBatch();
			preparedStmt.clearBatch();
			preparedStmt.close();
			System.gc();		
		}         	        
    }
	
	/**
	 * Acquire all the contents of the dictionary table into a HashMap
	 * 
	 * @param returningHashMap
	 * @return
	 * @throws SQLException
	 */
	public Map<Integer, String> getAllDictionaryValues(Map<Integer, String> returningHashMap) throws SQLException {
		
		int minCount = -2147482177;
		int maxCount = 2147482130;
		int step = (maxCount + (- minCount) + 1) / 1000;
		
		for (int stepCount = minCount; stepCount <= maxCount; stepCount+=step ) {
		
			if (connection != null) {
	
				String selectSQL = "SELECT stringValue, intValue FROM dictionary WHERE intValue > ? AND intValue < ?;";
				PreparedStatement preparedStmt = connection.prepareStatement(selectSQL);

				preparedStmt.setInt(1, stepCount);
				preparedStmt.setInt(2, stepCount + step);
									
				ResultSet rs = 	preparedStmt.executeQuery();
				
				while (rs.next()) {
					int intValue = rs.getInt("intValue");
					if (!returningHashMap.containsKey(intValue)) {
						returningHashMap.put(intValue, rs.getString("stringValue"));
					}
				}
				
				preparedStmt.clearBatch();
				preparedStmt.close();
				
			}
			System.gc();
		}		
		return returningHashMap;		
	}

	/**
	 * Attach all the dictionary values from the dictionary table into the current HashMap dictionary
	 * 
	 * @param resultsList
	 * @param returningHashMap
	 * @return
	 * @throws SQLException
	 */
	public Map<Integer, String> dictionaryFromDb(HashSet<Integer> resultsList, Map<Integer, String> returningHashMap) throws SQLException {
		 
		if (connection != null) {

			String selectSQL = "SELECT stringValue, intValue FROM dictionary WHERE intvalue = ? LIMIT 1;";
			PreparedStatement preparedStmt = connection.prepareStatement(selectSQL);

			for(int currentKey : resultsList) {
				
				if (currentKey == 992513052)
					Messenger.show("Found key 992513052");
				preparedStmt.setInt(1, currentKey);
				
				ResultSet rs = 	preparedStmt.executeQuery();
			
				int counter = 0;
				while (rs.next()) {
					int intValue = rs.getInt("intValue");
					
					if (!returningHashMap.containsKey(intValue)) {
						returningHashMap.put(intValue, rs.getString("stringValue"));
					}
					
					if (intValue == 992513052)
						Messenger.show("Found key 992513052 with value " + rs.getString("stringValue"));
					
					if (counter > 0 && counter % 10000 == 0)
						Messenger.show("Inserted " + counter);
				}
			}
			
			preparedStmt.clearBatch();
			preparedStmt.close();
			System.gc();	
		}         	        	
		
		return returningHashMap;
    }
	
	/**
	 * Drop all the triple and dictionary relations from the database
	 */
	public void dropSchema() {
			
		executeUpdate("DROP FUNCTION GetString;");
		
		for (String table : tables) {
			
			executeUpdate("DROP INDEX indexSubject" + table + ";");
			executeUpdate("DROP INDEX indexPredicate" + table + ";");
			executeUpdate("DROP INDEX indexObject" + table + ";");
			
			executeUpdate("DROP INDEX indexSubjectPredicate" + table + ";");
			executeUpdate("DROP INDEX indexSubjectObject" + table + ";");
			executeUpdate("DROP INDEX indexPredicateSubject" + table + ";");
			
			executeUpdate("DROP INDEX indexPredicateObject" + table + ";");
			executeUpdate("DROP INDEX indexObjectSubject" + table + ";");
			executeUpdate("DROP INDEX indexObjectPredicate" + table + ";");
			
			executeUpdate("DROP INDEX indexSubjectPredicateObject" + table + ";");
			executeUpdate("DROP INDEX indexSubjectObjectPredicate" + table + ";");
			executeUpdate("DROP INDEX indexPredicateSubjectObject" + table + ";");
			executeUpdate("DROP INDEX indexPredicateObjectSubject" + table + ";");
			executeUpdate("DROP INDEX indexObjectSubjectPredicate" + table + ";");
			executeUpdate("DROP INDEX indexObjectPredicateSubject" + table + ";");
			
			executeUpdate("DROP TABLE " + table + ";");
		}
		
		executeUpdate("DROP INDEX indexIntValue;");
		executeUpdate("DROP INDEX indexStringValue;");
		executeUpdate("DROP INDEX indexIntValueStringValue;");
		executeUpdate("DROP INDEX indexStringValueIntValue;");

		executeUpdate("DROP TABLE dictionary;");
	}
	
	/**
	 * Add the tables to the internal list
	 */
	public void generateTablesList() {		
		tables.add("tripleSPO");
	}
	
	/**
	 * Create the tables
	 */
	public void generateTables() {		
		executeUpdate("CREATE TABLE tripleSPO ("
				+ "subject INT, "
				+ "predicate INT, "
				+ "object INT"
				+ ");");
		
		executeUpdate("CREATE TABLE dictionary ("
				+ "intvalue INT, "
				+ "stringvalue TEXT"
				+ ");");
	}
	
	/**
	 * Create the database functions
	 */
	public void generateFunctions() {
		executeUpdate("CREATE FUNCTION GetString (input INTEGER) RETURNS TEXT "
				+ "BEGIN "
				+ "RETURN SELECT dictionary.stringvalue "
				+ "FROM dictionary "
				+ "WHERE intvalue=input LIMIT 1; "
				+ "END;");
	}
	
	public void cleanUpDictionary() {		
		executeUpdate("CREATE TABLE tripleSPO ("
				+ "subject INT, "
				+ "predicate INT, "
				+ "object INT"
				+ ");");
		
		executeUpdate("CREATE TABLE dictionary ("
				+ "intvalue INT, "
				+ "stringvalue TEXT"
				+ ");");
	}

	/**
	 * Generate schema indexes for all the triple and dictionary relations
	 */
	public void generateIndexes() {
		for (String table : tables) {
			executeUpdate("CREATE INDEX indexSubject" + table
					+ " ON " + table + " (subject);");
			
			executeUpdate("CREATE INDEX indexPredicate" + table
					+ " ON " + table + " (predicate);");
			
			executeUpdate("CREATE INDEX indexObject" + table
					+ " ON " + table + " (object);");
			
			executeUpdate("CREATE INDEX indexSubjectPredicate" + table
					+ " ON " + table + " (subject, predicate);");
			
			executeUpdate("CREATE INDEX indexSubjectObject" + table
					+ " ON " + table + " (subject, object);");
			
			executeUpdate("CREATE INDEX indexPredicateSubject" + table
					+ " ON " + table + " (predicate, subject);");
			
			executeUpdate("CREATE INDEX indexPredicateObject" + table
					+ " ON " + table + " (predicate, object);");
			
			executeUpdate("CREATE INDEX indexObjectSubject" + table
					+ " ON " + table + " (object, subject);");
			
			executeUpdate("CREATE INDEX indexObjectPredicate" + table
					+ " ON " + table + " (object, predicate);");
			
			// Triple composite indexes
			
			executeUpdate("CREATE INDEX indexSubjectPredicateObject" + table
					+ " ON " + table + " (subject, predicate, object);");			
			
			executeUpdate("CREATE INDEX indexSubjectObjectPredicate" + table
					+ " ON " + table + " (subject, object, predicate);");			
			
			executeUpdate("CREATE INDEX indexPredicateSubjectObject" + table
					+ " ON " + table + " (predicate, subject, object);");			
			
			executeUpdate("CREATE INDEX indexPredicateObjectSubject" + table
					+ " ON " + table + " (predicate, object, subject);");
			
			executeUpdate("CREATE INDEX indexObjectSubjectPredicate" + table
					+ " ON " + table + " (object, subject, predicate);");
			
			executeUpdate("CREATE INDEX indexObjectPredicateSubject" + table
					+ " ON " + table + " (object, predicate, subject);");			
		}
		
		executeUpdate("CREATE INDEX indexIntValue ON dictionary (intValue);");
		executeUpdate("CREATE INDEX indexStringValue ON dictionary (stringValue);");
		executeUpdate("CREATE INDEX indexIntValueStringValue ON dictionary (intValue, stringValue);");
		executeUpdate("CREATE INDEX indexStringValueIntValue ON dictionary (stringValue, intValue);");
	}
}
