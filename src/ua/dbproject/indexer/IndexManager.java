package ua.dbproject.indexer;

import java.sql.SQLException;
import java.util.ArrayList;

import ua.dbproject.dictionary.DbDictionary;

/**
 * The abstract class IndexManager takes care of organizing the index structures
 * by invoking the TTL parser and storing the resulting RDF index in appropriate
 * files on disk (or caches the index structures in main-memory if possible).
 */
public class IndexManager {

	private static IndexManager singletonInstance = null;
	
	/**
	 * This triple ArrayList serves as a temporary buffer for manual batching operations (MonetDB has issues with the embedded 
	 * Java batch operation, so this list along with a StringBuilder is used)
	 */	
	private ArrayList<Triple<Integer, Integer, Integer>> mainDict = new ArrayList<Triple<Integer, Integer, Integer>>(100000);
	//37 for all sizes, 0, 10.000, 100.000
	
	private static DbDictionary dbDictionary = DbDictionary.getInstance();
	
	private IndexManager() {
	}

	/**
	 * Access the Singelton's object instance
	 * 
	 * @return
	 */
	public static IndexManager getInstance() {
		if (singletonInstance == null)
			singletonInstance = new IndexManager();
		return singletonInstance;
	}
	
	/**
	 * Add triple to the main dictionary
	 * 
	 * @param inSubject
	 * @param inPredicate
	 * @param inObject
	 */
    private void addMainDict(int inSubject, int inPredicate, int inObject) {
        mainDict.add(new Triple<Integer, Integer, Integer>(inSubject, inPredicate, inObject));
	}

    /**
     * Clear the main dictionary
     */
	 public void flushMainDict() {
	     mainDict.clear();
	 }	

	/**
	 * Add a triple string onto the main dictionary
	 * 
	 * @param inSubject
	 * @param inPredicate
	 * @param inObject
	 * @throws SQLException
	 */
	public void add(String inSubject, String inPredicate, String inObject) throws SQLException {
        // using MapDB dictionary
        int intSubject = dbDictionary.addString(inSubject);
        int intPredicate = dbDictionary.addString(inPredicate);
        int intObject = dbDictionary.addString(inObject);
        
        addMainDict(intSubject, intPredicate, intObject);
	}	
	
	public int getMainDictCount() {
		return mainDict.size();
	}
	
	public ArrayList<Triple<Integer, Integer, Integer>> getMainDict() {
		return mainDict;
	}

	public long getCount() {
		return getMainDictCount();
	}

}