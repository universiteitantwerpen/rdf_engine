package ua.dbproject.indexer;

import java.io.Serializable;

/**
 * Data class/container, used only to store in RDF triplets within a compact form (like Tuple but for 3 string items
 * Recommended to store the Triple<S, P, O> object inside an ArrayList
 */
public final class Triple<S, P, O> implements Serializable  {
	
	/**
	 * This variable is needed for the Serializable interface
	 */
	private static final long serialVersionUID = 6167708198207331048L;
	private final S strSubject;
	private final P strPredicate;
	private final O strObject;
	
	/** 
	 * Default constructor for a Triple<S, P, O>
	 */
	public Triple(S inSubject, P inPredicate, O inObject) {
		this.strSubject = inSubject;
		this.strPredicate = inPredicate;
		this.strObject= inObject;
	}

	/**
	 * Get the subject
	 * 
	 * @return Object of type S
	 */
	public S getSubject() {
		return strSubject;
	}

	/**
	 * Get the predicate
	 * 
	 * @return Object of type P
	 */
	public P getPredicate() {
		return strPredicate;
	}

	/**
	 * Get the object
	 * 
	 * @return Object of type O
	 */
	public O getObject() {
		return strObject;
	}

}