package ua.dbproject.main;

import java.io.IOException;
import java.sql.SQLException;

import ua.dbproject.db.DbController;
import ua.dbproject.dictionary.DbDictionary;
import ua.dbproject.dictionary.DictUtility;
import ua.dbproject.optimizer.StopWatch;
import ua.dbproject.parsers.Messenger;
import ua.dbproject.processor.QueryProcessor;
import ua.dbproject.serializer.ObjectSerializerGZIP;

/**
 * This main class invokes the procedure of storing all the current dictionary values of the 
 * database into a serialized and compressed HashMap to disk
 */
public class ResultsToGZIP {

	protected static QueryProcessor queryProcessor;

	/**
	 * @param args
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws ClassNotFoundException, IOException, SQLException {

		StopWatch selectTimer = new StopWatch();
		ObjectSerializerGZIP objectSerializer = ObjectSerializerGZIP.getInstance();
		Messenger.setDebug(false);
		
		if (args.length > 0) {
		
			DbController dbController = new DbController();
			Messenger.setDebug(dbController.isDebugEnabled());
			
			DbDictionary dbDictionary = DbDictionary.getInstance();
			
			dbController.connect();
			
			selectTimer.start();
			
			dbDictionary.setDictionary(dbController.getAllDictionaryValues(dbDictionary.getMainDict()));
			
			objectSerializer.serializeHashDictionary(dbDictionary.getMainDict(), DictUtility.getCachePath(args[0]));
			
			selectTimer.stop();	
			
			Messenger.show("Total Dictionary Size: " + dbDictionary.getCount());				

			Messenger.show("Total Time: " + selectTimer.elapsedSeconds() + " seconds.");	
				
			dbController.close();						

			Messenger.show("Finished!\n\n");		
		} else {
			Messenger.show("No input SPARQL file specified.");
		}
	}
	
}
