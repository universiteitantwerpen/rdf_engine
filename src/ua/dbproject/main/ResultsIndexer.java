package ua.dbproject.main;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import ua.dbproject.db.DbController;
import ua.dbproject.indexer.IndexManager;
import ua.dbproject.optimizer.StopWatch;
import ua.dbproject.parsers.Messenger;
import ua.dbproject.parsers.InputParser;


/**
 * This main class performs indexing on files that contain variable number of rdf
 * items within each row
 */
public class ResultsIndexer {

	protected static IndexManager tripleHashMap;

	/**
	 *  
	 * @param args
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException {	

		StopWatch indexingTimer = new StopWatch();
		
		HashMap<Integer, Integer> queriesList = new HashMap<Integer, Integer>();
//		queriesList.put(1, 4);
//		queriesList.put(2, 1);
//		queriesList.put(3, 3);
//		queriesList.put(4, 1);
//		queriesList.put(5, 5);
//		queriesList.put(6, 3);
//		queriesList.put(7, 4);
//		queriesList.put(8, 5);
//		queriesList.put(9, 4);
//		queriesList.put(10, 4);
//		queriesList.put(11, 6);
//		queriesList.put(12, 3);
//		queriesList.put(13, 2);
//		queriesList.put(14, 3);
//		queriesList.put(15, 3);
//		queriesList.put(16, 7);
//		queriesList.put(17, 8);
//		queriesList.put(18, 4);
//		queriesList.put(19, 7);
		queriesList.put(20, 9);
//		queriesList.put(21, 5);
//		queriesList.put(22, 2);
//		queriesList.put(23, 6);
//		queriesList.put(24, 3);
//		queriesList.put(25, 3);
//		queriesList.put(26, 4);
//		queriesList.put(27, 7);
//		queriesList.put(28, 3);
//		queriesList.put(29, 9);
//		queriesList.put(30, 17);
//		queriesList.put(31, 4);
//		queriesList.put(32, 2);
		
		DbController dbController = new DbController();
		Messenger.setDebug(true);
		
		Messenger.show("Starting Indexer...");	
		
		Messenger.show("Connecting with " + dbController.getDbEngine() + "...");

		dbController.connect();

		dbController.generateTablesList();			
		
		Messenger.show("Dropping schema...");
		dbController.dropSchema();

		Messenger.show("Generating tables...");	
		dbController.generateTables();
		
		Messenger.show("Generating functions...");	
		dbController.generateFunctions();
					
		Messenger.show("Generating indexes...");				
		dbController.generateIndexes();
		
		indexingTimer.start();
		
		Messenger.show("Started inserting rows...");
		
		int minI = Integer.MAX_VALUE;
		int maxI = Integer.MIN_VALUE;
		
		for ( int key : queriesList.keySet() ) {
		    if (key < minI)
		    	minI = key;
		    if (key > maxI)
		    	maxI = key;
		}
		
		for (int i = minI; i <= maxI; i++) {
			
			if (queriesList.containsKey(i)) {
				dbController.connect();
				Messenger.show("Opening q" + i + "...");
				InputParser.readInputStoreToDb("/Users/ktheodorakos/Downloads/results/q" + i + ".txt", dbController, queriesList.get(i));
	
				dbController.close();
			}
		}

		indexingTimer.stop();

		Messenger.show("Done in: ");
		System.out.println(indexingTimer.elapsedSeconds());
		Messenger.show(" seconds");
	}

}
