package ua.dbproject.main;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import ua.dbproject.db.DbController;
import ua.dbproject.dictionary.DbDictionary;
import ua.dbproject.dictionary.DictUtility;
import ua.dbproject.optimizer.StopWatch;
import ua.dbproject.parsers.FileHandler;
import ua.dbproject.parsers.Messenger;
import ua.dbproject.parsers.SPARQL_Parser;
import ua.dbproject.parsers.SparqlQuery;
import ua.dbproject.processor.QueryProcessor;
import ua.dbproject.serializer.ObjectSerializerGZIP;

/**
 * This main class invokes the entire query processing workflow. The SPARQL
 * query(s) should be provided as one or more command-line arguments.
 */
public class SPARQL_Processor {

	protected static QueryProcessor queryProcessor;

	/**
	 * @param args
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws ClassNotFoundException, IOException, SQLException {

		StopWatch selectTimer = new StopWatch();
		boolean warmUp = false;
		boolean blnAllQueries = false;
		boolean blnNt = false;
		boolean blnNtSample = false;
		boolean dbDict = false;
		
		String resultsQuerying = "";
		int minQuery = 1;
		int maxQuery = 32;
		
		int initArgument = 0;
		ObjectSerializerGZIP objectSerializer = ObjectSerializerGZIP.getInstance();
		
		if (args.length > 0) {
		
			DbController dbController = new DbController();
			Messenger.setDebug(dbController.isDebugEnabled());
						
			DbDictionary dbDictionary = DbDictionary.getInstance();

			HashSet<Integer> intHashSetResults = new HashSet<Integer>();
						
			Messenger.show("Using database: " + dbController.getDbEngine());
			
			if (args[0].equals("-wall")) {
				args[0] = "-all";
				Messenger.show("Warm up round for all queries");
				warmUp = true;
				initArgument++;
			} else if (args[0].equals("-wallSample")) {
				args[0] = "-allSample";
				Messenger.show("Warm up round for sample queries");
				warmUp = true;
				initArgument++;
			}
			
			if (args[0].equals("-all") || args[0].equals("-allSample")) {
				blnAllQueries = true;
				initArgument++;
				
				String startingQuery = "";
				String endingQuery = "";
				
				if (args.length == 3) {
					startingQuery = args[1];
					endingQuery = args[2];
					
					minQuery = Integer.parseInt(startingQuery);
					maxQuery = Integer.parseInt(endingQuery);
				}
				
				for (int i = minQuery; i <= maxQuery; i++)
					resultsQuerying += i + "\t";
				
				resultsQuerying += "\n";
				
				String[] allQueries = null;
				
				if (args[0].equals("-all")) {
					blnNt = true;
					allQueries = new String[] {
							"-all",
							"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o0 .?o0 <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?o1 <http://rdf.freebase.com/ns/film.performance.film> ?o2 .}",
							"SELECT DISTINCT ?s WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o0 .?o0 <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?o1 <http://rdf.freebase.com/ns/film.performance.film> ?o2 .}",
							"SELECT ?s, ?o1, ?o2 WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .}",
							"SELECT DISTINCT ?s WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .}",
							"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o4 .?o4 <http://rdf.freebase.com/ns/film.performance.film> ?o5 .}",//6
							"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .}",
							"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o0 .?o0 <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?o1 <http://rdf.freebase.com/ns/film.performance.film> ?o2 .}",
							"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o4 .?o4 <http://rdf.freebase.com/ns/film.performance.film> ?o5 .}",//9
							"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/common.topic.article> ?o0 .?s <http://creativecommons.org/ns#attributionName> ?o1 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o2 .}",
							"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film.starring> ?o0 .?o0 <http://rdf.freebase.com/ns/film.performance.film> ?o1 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o2 .}",
							"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/common.topic.article> ?o0 .?s <http://creativecommons.org/ns#attributionName> ?o1 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o2 .?o2 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .?o4 <http://rdf.freebase.com/ns/film.film.starring> ?o5 .}",
							"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/aviation.aircraft_owner> .?s <http://www.w3.org/2002/07/owl#sameAs> ?o1 .?s <http://rdf.freebase.com/ns/type.object.name> ?o2 .}",
							"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/common.topic.webpage> ?o0 .}",
							"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/aviation.aircraft_owner> .?s <http://www.w3.org/2002/07/owl#sameAs> ?o1 .?s <http://rdf.freebase.com/ns/type.object.name> ?o2 .?s <http://rdf.freebase.com/ns/common.topic.webpage> ?o3 .}",
							"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .}",
							"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o0 .?o0 <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?o1 <http://rdf.freebase.com/ns/film.performance.film> ?o2 .?o2 <http://rdf.freebase.com/ns/film.film.starring> ?o3 .?o3 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .?o4 <http://rdf.freebase.com/ns/film.film.starring> ?o5 .}",
							"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o4 .?o4 <http://rdf.freebase.com/ns/film.performance.film> ?o5 .?o5 <http://rdf.freebase.com/ns/film.film.starring> ?o6 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o7 .?o7 <http://rdf.freebase.com/ns/film.film.starring> ?o8 .}",
							"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film.genre> ?o0 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?s <http://www.w3.org/2002/07/owl#sameAs> ?o2 .}",
							"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film.starring> ?o0 .?o0 <http://rdf.freebase.com/ns/film.performance.film> ?o1 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o2 .?o2 <http://rdf.freebase.com/ns/film.performance.film> ?o3 .?o3 <http://rdf.freebase.com/ns/film.film.starring> ?o4 .?o4 <http://rdf.freebase.com/ns/film.performance.film> ?o5 .}",
							"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film.genre> ?o0 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?s <http://www.w3.org/2002/07/owl#sameAs> ?o2 .?o1 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .?o4 <http://rdf.freebase.com/ns/film.film.starring> ?o5 .?o5 <http://rdf.freebase.com/ns/film.performance.film> ?o6 .?o6 <http://rdf.freebase.com/ns/film.film.starring> ?o7 .?o7 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .}",
							"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/common.topic> .?s <http://rdf.freebase.com/ns/location.location.geolocation> ?o1 .?s <http://creativecommons.org/ns#attributionURL> ?o2 .?s <http://rdf.freebase.com/ns/type.object.name> ?o3 .?s <http://rdf.freebase.com/ns/location.location.containedby> ?o4 .}",
							"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/location.location.geolocation> ?o0 .}",
							"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/common.topic> .?s <http://rdf.freebase.com/ns/location.location.geolocation> ?o1 .?s <http://creativecommons.org/ns#attributionURL> ?o2 .?s <http://rdf.freebase.com/ns/type.object.name> ?o3 .?s <http://rdf.freebase.com/ns/location.location.containedby> ?o4 .?s <http://rdf.freebase.com/ns/type.object.name> ?o5 .}",
							"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .}",
							"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o0 .?o0 <http://rdf.freebase.com/ns/film.film.starring> ?o1 .}",
							"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o4 .}",
							"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/common.topic.image> ?o0 .?s <http://creativecommons.org/ns#attributionURL> ?o1 .?s <http://rdf.freebase.com/ns/type.object.name> ?o2 .?s <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o4 .?s <http://creativecommons.org/ns#attributionName> ?o5 .}",
							"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film.starring> ?o0 .?o0 <http://rdf.freebase.com/ns/film.performance.film> ?o1 .}",
							"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/common.topic.image> ?o0 .?s <http://creativecommons.org/ns#attributionURL> ?o1 .?s <http://rdf.freebase.com/ns/type.object.name> ?o2 .?s <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o4 .?s <http://creativecommons.org/ns#attributionName> ?o5 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o6 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .}",
							"SELECT * WHERE { ?s1 <http://rdf.freebase.com/ns/common.topic.image> ?o0 .?s1 <http://creativecommons.org/ns#attributionURL> ?o1 .?s1 <http://rdf.freebase.com/ns/type.object.name> ?o2 .?s1 <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s1 <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o4 .?s1 <http://creativecommons.org/ns#attributionName> ?o5 .?s1 <http://rdf.freebase.com/ns/film.film.starring> ?o6 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .?s2 <http://rdf.freebase.com/ns/common.topic.image> ?o9 .?s2 <http://creativecommons.org/ns#attributionURL> ?o10 .?s2 <http://rdf.freebase.com/ns/type.object.name> ?o11 .?s2 <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s2 <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o12 .?s2 <http://creativecommons.org/ns#attributionName> ?o13 .?s2 <http://rdf.freebase.com/ns/film.film.starring> ?o14 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o15 .}",
							"SELECT DISTINCT ?s1, ?s2, ?o8, ?o15 WHERE { ?s1 <http://rdf.freebase.com/ns/common.topic.image> ?o0 .?s1 <http://creativecommons.org/ns#attributionURL> ?o1 .?s1 <http://rdf.freebase.com/ns/type.object.name> ?o2 .?s1 <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s1 <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o4 .?s1 <http://creativecommons.org/ns#attributionName> ?o5 .?s1 <http://rdf.freebase.com/ns/film.film.starring> ?o6 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .?s2 <http://rdf.freebase.com/ns/common.topic.image> ?o9 .?s2 <http://creativecommons.org/ns#attributionURL> ?o10 .?s2 <http://rdf.freebase.com/ns/type.object.name> ?o11 .?s2 <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s2 <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o12 .?s2 <http://creativecommons.org/ns#attributionName> ?o13 .?s2 <http://rdf.freebase.com/ns/film.film.starring> ?o14 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o15 .}",
							"SELECT DISTINCT ?o8, ?o15 WHERE { ?s1 <http://rdf.freebase.com/ns/common.topic.image> ?o0 .?s1 <http://creativecommons.org/ns#attributionURL> ?o1 .?s1 <http://rdf.freebase.com/ns/type.object.name> ?o2 .?s1 <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s1 <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o4 .?s1 <http://creativecommons.org/ns#attributionName> ?o5 .?s1 <http://rdf.freebase.com/ns/film.film.starring> ?o6 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .?s2 <http://rdf.freebase.com/ns/common.topic.image> ?o9 .?s2 <http://creativecommons.org/ns#attributionURL> ?o10 .?s2 <http://rdf.freebase.com/ns/type.object.name> ?o11 .?s2 <http://rdf.freebase.com/ns/film.film.directed_by> ?o3 .?s2 <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o12 .?s2 <http://creativecommons.org/ns#attributionName> ?o13 .?s2 <http://rdf.freebase.com/ns/film.film.starring> ?o14 .?o6 <http://rdf.freebase.com/ns/film.performance.film> ?o15 .}",
							};
				} else if (args[0].equals("-allSample")) {
					blnNtSample = true;
					allQueries = new String[] {
							"-allSample",
							"SELECT ?s, ?o WHERE { ?s <http://rdf.freebase.com/ns/common.topic.webpage> ?o .}",
							"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/location.location.geolocation> ?o .}",
							"SELECT ?s, ?o1, ?o2 WHERE { ?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o2 .}",
							"SELECT ?s, ?o1, ?o2 WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .}",
							"SELECT * WHERE { ?s <http://rdf.freebase.com/ns/film.film.genre> ?o0 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o1 .?s <http://www.w3.org/2002/07/owl#sameAs> ?o2 .}",
							"SELECT DISTINCT ?o1, ?o2 WHERE {?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o2 .?o2 <http://rdf.freebase.com/ns/film.performance.film> ?o3 .}",
							"SELECT ?s, ?o1, ?o2, ?o3, ?o4 WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o3 .?o3 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .}",
							"SELECT DISTINCT ?s, ?o5 WHERE { ?s <http://rdf.freebase.com/ns/common.topic.article> ?o1 .?s <http://creativecommons.org/ns#attributionName> ?o2 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o3 .?o3 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .?o4 <http://rdf.freebase.com/ns/film.film.starring> ?o5 .}",
							"SELECT * WHERE { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdf.freebase.com/ns/film.film_cut> .?s <http://rdf.freebase.com/ns/film.film_cut.film> ?o1 .?s <http://rdf.freebase.com/ns/film.film_cut.runtime> ?o2 .?o1 <http://rdf.freebase.com/ns/film.film.starring> ?o3 .?o3 <http://rdf.freebase.com/ns/film.performance.film> ?o4 .?o4 <http://rdf.freebase.com/ns/film.film.starring> ?o5 .?o5 <http://rdf.freebase.com/ns/film.performance.film> ?o6 .?o6 <http://rdf.freebase.com/ns/film.film.starring> ?o7 .}",
							"SELECT DISTINCT ?s, ?o8 WHERE {?s <http://rdf.freebase.com/ns/common.topic.image> ?o1 .?s <http://creativecommons.org/ns#attributionURL> ?o2 .?s <http://rdf.freebase.com/ns/type.object.name> ?o3 .?s <http://rdf.freebase.com/ns/film.film.directed_by> ?o4 .?s <http://rdf.freebase.com/ns/film.film.costume_design_by> ?o5 .?s <http://creativecommons.org/ns#attributionName> ?o6 .?s <http://rdf.freebase.com/ns/film.film.starring> ?o7 .?o7 <http://rdf.freebase.com/ns/film.performance.film> ?o8 .}"
							};
				}
				
				args = allQueries;
			}
			
			for (int i = initArgument; i < args.length; i++) {
				
				if (i >= minQuery && i <= maxQuery) {
			
					dbController.connect();
					
					selectTimer.start();
					
					String query = args[i];
									
					String currentDict = "rDictionary1-15";
					
					if (blnAllQueries && blnNt) {
						if (i == 22 || i == 31 || i == 32 || (i >= 24 && i <= 29))
							currentDict = "rDictionary22_24-29_31-32";						
						else if (i >= 1 && i <= 15 || i == 20 || i == 30)
							currentDict = "rDictionary1-15";						
						else
							currentDict = "rDictionary" + i;
					} else if (blnAllQueries && blnNtSample) {
						if (i >= 2 && i <= 8) {
							currentDict = "rSampleDictionary" + i;
						}
					}
					
					if (!warmUp) {
						
						Messenger.show("Loading warmed up dictionary...");
						Messenger.show("Current i: " + i);
						Messenger.show("Currently chosen dictionary: " + currentDict);
						Messenger.show("Cache dict path: " + DictUtility.getCachePath(currentDict));
						
						// Load warmed up dictionary from disk
						dbDictionary.setDictionary(objectSerializer.deserializeHashDictionary(DictUtility.getCachePath(currentDict)));
						
						// If the warmed up dictionary doesn't exist, use a DbDictionary
						if (dbDictionary.getCount() == 0) {						
							Messenger.show("No dictionary cache detected, loading from db");	
							dbDict = true;
//							dbDictionary.setDictionary(dbController.getAllDictionaryValues(dbDictionary.getMainDict()));
//							dbDictionary.setDictionary(dbController.dictionaryFromDb(intHashSetResults, dbDictionary.getMainDict()));
						} else {					
							Messenger.show("Loaded warmup dict from path:" + DictUtility.getCachePath(currentDict));						
						}					
					}
					
					// do parse the query
					SPARQL_Parser rdfParser = new SPARQL_Parser();						
					SparqlQuery rdfQuery = new SparqlQuery(rdfParser.getRdfVariables(query), rdfParser.getRdfJoins(query), rdfParser.isDistinct(query));
							
					// do send the produced query to the db
					Messenger.show("Getting SQL results...");
					
					// Transform SPARQL to SQL
					Messenger.show("Transforming SPARQL to SQL...");				
					Messenger.show(rdfQuery.toSQL());
					
					ArrayList<String[]> results = dbController.getResultsArrayList(rdfQuery.toSQL(), rdfQuery.getColumnCount());				
							
					Messenger.show("Received " + results.size() + " rows in " + selectTimer.elapsedIntervalMilliSeconds() + " ms.");	
										
					if (dbDict) {
						for(String[] array : results) {
							for (String str : array) {
								int currentInt = Integer.parseInt(str);
								intHashSetResults.add(currentInt);
							}
						}

						Messenger.show("HashSet size: " + intHashSetResults.size());
						
						// populate dbdict with hashset map items
						Messenger.show("Populating Db dictionary...");
						dbDictionary.setDictionary(dbController.dictionaryFromDb(intHashSetResults, dbDictionary.getMainDict()));
						
						//temp
						Messenger.show("Storing warmup dict to path:" + DictUtility.getCachePath("currentDictionary" + i));
						objectSerializer.serializeHashDictionary(dbDictionary.getMainDict(), DictUtility.getCachePath("rFbSample" + i));
					}
					
					if(!warmUp && !blnAllQueries) {

						for(String[] array : results) {
							for (String str : array) {
								System.out.print(dbDictionary.getString(Integer.parseInt(str)) + " ");							
							}
							System.out.println("\n");
						}
						
					} else if (!warmUp && blnAllQueries) {
						
						Messenger.show("Storing results to a file...");
				
						StringBuilder outputStringBuilder = new StringBuilder();
						
						for(String[] array : results) {
							for (String str : array) {
								outputStringBuilder.append(dbDictionary.getString(Integer.parseInt(str)) + " ");
							}
							outputStringBuilder.append("\n");
						}		
						
						String outputFileName = "q" + i + ".txt";
						if (blnNtSample)
							outputFileName = "qSample" + i + ".txt";
						
						FileHandler.writeFile(outputStringBuilder, outputFileName);
						
					} else if (warmUp) {				
						for(String[] array : results) {
							for (String str : array) {
								int currentInt = Integer.parseInt(str);
								intHashSetResults.add(currentInt);
							}
						}
					}
										
					String resultsAcquisitionTime = Long.toString(selectTimer.elapsedIntervalMilliSeconds());
					Messenger.show("Acquired result list in "+ resultsAcquisitionTime + " ms.");				
		
					// Store the results locally
	//				FileHandler.writeFile(outputString, "output.txt");
	//				FileHandler.writeFile(sb, "output.txt");// speed 87 sec
					
					if (warmUp) {
						Messenger.show("Initiated procedure to acquire the DbDictionary...");	
						Map<Integer, String> currentDictionary = new HashMap<Integer, String>();
						// Populate a new HashMap
						currentDictionary = dbController.dictionaryFromDb(intHashSetResults, currentDictionary);
						// Store current Dictionary(i) to Disk as a serialized object
						String dictName = "rDictionary" + i;
						
						if (blnNtSample)
							dictName = "rSampleDictionary" + i;
						Messenger.show("Storing warmup dict to path:" + DictUtility.getCachePath(dictName + i));
						
						objectSerializer.serializeHashDictionary(currentDictionary, DictUtility.getCachePath(dictName + i));
					}
					
					selectTimer.stop();	
					String totalTime = Long.toString(selectTimer.elapsed());
					
					Messenger.show("Total Dictionary Size: " + dbDictionary.getCount());				
					Messenger.show("\n\nDone " + i);		
					Messenger.show("Total Time for " + i + ": " + totalTime + " ms.\n\n");	
						
					dbController.close();
					
					if (blnAllQueries) {
						resultsQuerying += resultsAcquisitionTime + "ms, " + totalTime + "ms\t";
					}	
				}
				
				FileHandler.writeFile(resultsQuerying, "ResultsQuerying.txt");
			}
			
			Messenger.show("Finished!\n\n");	
			
		} else {
			Messenger.show("No input SPARQL file specified.");
		}
	}
}
