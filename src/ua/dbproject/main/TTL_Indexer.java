package ua.dbproject.main;
import java.io.IOException;
import java.sql.SQLException;

import ua.dbproject.db.DbController;
import ua.dbproject.indexer.IndexManager;
import ua.dbproject.optimizer.StopWatch;
import ua.dbproject.parsers.Messenger;
import ua.dbproject.parsers.InputParser;


/**
 * This main class invokes the entire indexing workflow. A file or directory with the
 * TTL files should be provided as command-line arguments.
 */
public class TTL_Indexer {

	protected static IndexManager tripleHashMap;

	/**
	 *  
	 * @param args
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException {	

		StopWatch indexingTimer = new StopWatch();
		
		if (args.length > 0) {
		
			DbController dbController = new DbController();
			Messenger.setDebug(dbController.isDebugEnabled());
			
			Messenger.show("Starting Indexer...");			
			Messenger.show("Connecting with " + dbController.getDbEngine() + "...");

			dbController.connect();
			dbController.generateTablesList();			
			
			Messenger.show("Dropping schema...");
			dbController.dropSchema();

			Messenger.show("Generating tables...");	
			dbController.generateTables();
			
			Messenger.show("Generating functions...");	
			dbController.generateFunctions();
						
			if (dbController.getDbEngine().equals("monetdb")) {
				Messenger.show("Generating indexes...");				
				dbController.generateIndexes();
			}
			
			indexingTimer.start();
			
			Messenger.show("Started inserting rows...");
			
			for (int i = 0; i < args.length; i++) {
				dbController.connect();
				Messenger.show("Opening " + args[i] + "...");
				InputParser.readInputStoreToDb(args[i], dbController);
				dbController.close();
			}

			// If database is Postgresql, do create the indexes after the 
			// end of the insertion
			if (dbController.getDbEngine().equals("postgresql")) {
				StopWatch indexingTime = new StopWatch();
				indexingTime.start();
				dbController.connect();
				
				Messenger.show("Generating indexes...");				
				dbController.generateIndexes();
				dbController.close();
				indexingTime.stop();
				Messenger.show("Performed indexing in " + indexingTime.elapsed() + "ms.");			
			}
			
			indexingTimer.stop();

			Messenger.show("Done in: ");
			System.out.print(indexingTimer.elapsed());
			Messenger.showSameLine(" ms.");
		} else {
			System.out.println("No input file specified.");
		}
	}
}
