package ua.dbproject.processor;

public class MyQueryProcessor extends QueryProcessor {

	@SuppressWarnings("unused")
	private String message = null;
	
	private String processQuery(String query) {

		query += " -> start process";
		
		return query;
	}
	
	public MyQueryProcessor() {
		
	}
	
	public MyQueryProcessor(String inMessage) {
		this.message = inMessage;
	}
	
	@Override
	public QueryResult processQuery(String[] args) {
		return new MyQueryResult(this.processQuery(args[0]));
	}

}
