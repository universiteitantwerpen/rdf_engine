package ua.dbproject.statistics;

import java.io.File;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import ua.dbproject.db.DbParameters;

public class SettingsReader {
	
	private String currentDb = "";
	private String databaseSchemaName = "";
	private String databaseUserName = "";
	private String databasePassword = "";
	private String jDBCDriver = "";
	private String port = "";
	private String ip = "";
	private boolean debug = false;
	
	/**
	 * If debug is disabled, then no messages are being shown
	 * 
	 * @return
	 */
	public boolean isDebugEnabled() {
		return this.debug;
	}
	
	/**
	 * Automatically read the current db settings from the xml settings file
	 * 
	 * @return
	 */
	public DbParameters getXmlSettings() {

		DbParameters returningParameters = null;
		
		try {
			
			// Open the settings file
			File fXmlFile = new File("settings.xml");
			
			// Create an xml reader instance
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			org.w3c.dom.Document doc = dBuilder.parse(fXmlFile);
		 
			doc.getDocumentElement().normalize();
			
			//If the root element is "dbSettings"
			if ("dbSettings".equals(doc.getDocumentElement().getNodeName())) {
	
				// Currently selected db - Default monetdb
				this.currentDb = doc.getElementsByTagName("currentDb").item(0).getTextContent();
				
				// Check if debug mode is enabled, aka showing output messages
				this.debug = "true".equals(doc.getElementsByTagName("debug").item(0).getTextContent()) ? true : false;
				
				NodeList nList = doc.getElementsByTagName("db");
			 		 
				// Browse all the db settings
				for (int temp = 0; temp < nList.getLength(); temp++) {
			 				
					Node nNode = nList.item(temp);
			 
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
			 
						Element eElement = (Element) nNode;
			
						// Do return the current db's settings
						if (currentDb.equals(eElement.getAttribute("id"))) {
						
							this.currentDb = eElement.getAttribute("id");						
							this.databaseSchemaName = eElement.getElementsByTagName("databaseSchemaName").item(0).getTextContent();
							this.databaseUserName = eElement.getElementsByTagName("databaseUserName").item(0).getTextContent();
							this.databasePassword = eElement.getElementsByTagName("databasePassword").item(0).getTextContent();
							this.jDBCDriver = eElement.getElementsByTagName("jDBCDriver").item(0).getTextContent();
							this.port = eElement.getElementsByTagName("port").item(0).getTextContent();
							this.ip = eElement.getElementsByTagName("ip").item(0).getTextContent();
							
							returningParameters = new DbParameters(this.jDBCDriver, this.currentDb, this.ip, this.port, this.databaseSchemaName, this.databaseUserName, this.databasePassword);
						}
					}
				}
			}
		} catch (Exception ex) { 		
			// In case of any exception, return the default monetdb db settings
			returningParameters = new DbParameters("nl.cwi.monetdb.jdbc.MonetDriver", "monetdb", "127.0.0.1", "50000", "dbProject", "monetdb", "monetdb");
		}
		return returningParameters;
	}
}
