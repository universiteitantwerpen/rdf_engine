package ua.dbproject.dictionary;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Parent abstract class that allows to access different algorithms for
 * many types of Dictionaries. Uses Strategy pattern 
 * 
 * @author madks_000
 *
 */
public abstract class GenericDictionary {
	
	protected IDictionaryTypeBehavior dictionaryTypeBehavior;
	
	// Public parameters for all the dictionaries
	public static int TTLFileCounter = 0;
	public static int DictionaryFileCounter = 0;
	public final static int RDFChunkSize = 100000;
	public final static long MaxSize = Long.MAX_VALUE;
	public final static int MessageEvery = 100000;
	public final static int DictionaryEveryTripleRecords = 10000000;
	
	public void setSerializeBehavior(IDictionaryTypeBehavior dtb) {
		dictionaryTypeBehavior = dtb;
	}
	
	public int addString(String inString) {
		return dictionaryTypeBehavior.addString(inString);
	}
	
	/**
	 * Clear the contents of the dictionary
	 */
	public void flush() {
		dictionaryTypeBehavior.flush();
	}
	
	/**
	 * Get the number of the added strings within the dictionary
	 * 
	 * @return
	 */
	public int getCount() {
		return dictionaryTypeBehavior.getCount();
	}
	
	/**
	 * Get the string value for a specific integer (hashCode)
	 */
	public String getString(int inIndex) {
		return dictionaryTypeBehavior.getString(inIndex);
	}
	
	/**
	 * Get the integer value  of a specific string, without storing
	 * it in the dictionary
	 * 
	 * @param strIn
	 * @return
	 */
	public int getInt(String strIn) {
		return dictionaryTypeBehavior.getInt(strIn);
	}
	
	/**
	 * Read/Deserialize a dictionary from the disk to the memory
	 */
	public void loadFromDisk(String path) throws ClassNotFoundException, IOException {
		dictionaryTypeBehavior.loadFromDisk(path);
	}
	
	/**
	 * Write/Serialize a dictionary and store it to disk
	 */
	public void toDisk(String pathToSave) throws FileNotFoundException, IOException {
		dictionaryTypeBehavior.toDisk(pathToSave);	
	}	
}
