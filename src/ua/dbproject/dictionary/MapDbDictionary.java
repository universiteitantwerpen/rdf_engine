package ua.dbproject.dictionary;

import java.sql.SQLException;

import ua.dbproject.db.DbController;

/**
 * HashDictionary uses Singleton instance using the HashMapType behavior (Stategy Pattern) 
 * @version 0.1
 * @author madks_000
 */
public final class MapDbDictionary extends GenericDictionary {
	
	private static MapDbDictionary uniqueInstance = null;
	
	/**
	 * Private constructor
	 */
	private MapDbDictionary() {
		dictionaryTypeBehavior = new MapDbType();
	}
	
	/**
	 * Acquire the unique instance of the HashDictionary using lazy instantiation
	 * 
	 * @return the unique instance
	 */
	public static MapDbDictionary getInstance() {
		if(uniqueInstance == null)
			uniqueInstance = new MapDbDictionary();
		return uniqueInstance;
	}

	public void close() {
		// TODO Auto-generated method stub
		
	}

	public void memToDiskMap(DbController db) throws SQLException {
		dictionaryTypeBehavior.toPersistentStorage(db);		
	}	
}
