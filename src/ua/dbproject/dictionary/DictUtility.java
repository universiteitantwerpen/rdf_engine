package ua.dbproject.dictionary;

import java.io.IOException;

/** 
 * Static class that helps in string conversions
 */
public class DictUtility {
	
	/** 
	 * Strip all filesystem-illegal characters
	 */
	public static String removeInvalidFsChars(String inString) {
		return inString.replaceAll("[^a-zA-Z0-9.-]", "");
	}

	public static String removeConstantPrefixes(String inString) {
		return inString.replaceAll("\"", "");
	}
	
	public static String processNT(String inString) {
		return removeConstantPrefixes(removeEqualityChars(inString));
	}
	
	/**
	 * Detect and remove website prefixes
	 * 
	 * @param inString
	 * @return
	 */
	public static String removeAddressChars(String inString) {
		return inString.replaceAll("http://", "").replaceAll("https://", "").replaceAll("ftp://","")
				.replaceAll("www.","");
	}
	
	/**
	 * Remove the xsd part of the read string
	 * 
	 * @param inString
	 * @return
	 */
	public static String removeXsd(String inString) {
		if (inString.matches(".*(\\^\\^xsd\\:).*")) {
			String[] regexArray = inString.split("\"\\^\\^xsd:.");
			inString = regexArray[0].substring(1);
		}
		return inString;		
	}

	/** 
	 * Remove <, > characters along with stripping file system
	 * naming illegal characters
	 * */
	public static String stripToValidPathString(String inString) {
		return removeEqualityChars(removeInvalidFsChars(removeAddressChars(inString)));
	}
    	
	/**
	 * Remove the dot character, used for the object string
	 * 
	 * @param inString
	 * @return
	 */
	public static String removeDot(String inString) {
		return inString.replace(" .", "");
	}
	
	/**
	 * Remove the < and > characters
	 * 
	 * @param inString
	 * @return
	 */
	public static String removeEqualityChars(String inString) {
		return inString.replace("<", "").replace(">", "");
	}
	
	/**
	 * Helps in checking wether the read line is a prefix and not
	 * actual RDF data
	 * 
	 * @param inString
	 * @return
	 */
	public static boolean isTTLPrefix(String inString) {
		if (inString.equals("@base") || inString.equals("@prefix") || inString.equals("#@") || inString.equals("#"))
			return true;
		else
			return false;
	}
	
	/**
	 * Get the file system path for the cache storage
	 * 
	 * @param extraPath
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static String getCachePath(String extraPath) throws ClassNotFoundException, IOException {
		return getPathToSave() + extraPath;
	}	
	
	/**
	 * Get the absolute file system path 
	 * 
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static String getPathToSave() throws ClassNotFoundException, IOException {
		return "cache/";
	}
}
