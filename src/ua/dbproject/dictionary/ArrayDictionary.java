package ua.dbproject.dictionary;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import ua.dbproject.serializer.ObjectSerializerGZIP;

public class ArrayDictionary {
	
	/**
	 * The ArrayList that stores all the strings
	 */
	private static ArrayList<String> stringList = new ArrayList<String>();
	
	/**
	 * Get the integer hashCode value of a string
	 * without storing it within the dictionary
	 */
	public static int addStringOneWay(String inString) {
		return inString.hashCode();
	}
	
	/**
	 * Clear all of the dictionary's elements
	 */
	public static void flush() {
		stringList.clear();
	}
	
	/**
	 * Get how many elements are stored within the dictionary
	 */
	public static int getCount() {
		return stringList.size();
	}
	
	/**
	 * Get the string value for a specific integer (hashCode)
	 */
	public static String getString(int inIndex) {
		return stringList.get(inIndex);
	}
	
	/**
	 * Write/Serialize a dictionary and store it to disk
	 */
	public static void ToDisk(String pathToSave) throws FileNotFoundException, IOException {
		ObjectSerializerGZIP arrayDictionarySerializer = ObjectSerializerGZIP.getInstance(); 
		arrayDictionarySerializer.serialize(stringList, pathToSave);		
	}	
}
