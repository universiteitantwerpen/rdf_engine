package ua.dbproject.dictionary;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import ua.dbproject.db.DbController;
import ua.dbproject.serializer.ObjectSerializerGZIP;

/**
 * Static class that contains the algorithm for a HashMap type
 * of dictionary. Part of Strategy pattern
 * 
 * @author madks_000
 *
 */
public class HashMapType implements IDictionaryTypeBehavior{

	private static Map<Integer, String> intToString = new HashMap<Integer, String>();
	/**
	 * Adds a string in the dictionary.
	 * 
	 * @return Returns Java's hashCode for this string value
	 */
	public int addString(String inString) {
		int returnInt = inString.hashCode();
		
		if(!intToString.containsKey(inString)) {
			intToString.put(returnInt, inString);
		}
		return returnInt;		
	}
	
	/**
	 * Get the integer hashCode value of a string
	 * without storing it within the dictionary
	 */
	public int addStringOneWay(String inString) {
		return getInt(inString);
	}
	
	/**
	 * Clear all of the dictionary's elements
	 */
	public void flush() {
		intToString.clear();
	}
		
	/**
	 * Get how many elements are stored within the dictionary
	 */
	public int getCount() {
		return intToString.size();
	}
	
	/**
	 * Get the string value for a specific integer (hashCode)
	 */
	public String getString(int inIndex) {
		return intToString.get(inIndex);
	}

	/**
	 * Get the hashcode of a specific string, without storing
	 * it in the dictionary
	 * 
	 * @param strIn
	 * @return
	 */
	public int getInt(String strIn) {
		return strIn.hashCode();
	}
	
	/**
	 * Get the instance of the HashMap dictionary that stores
	 * all the dictionary values
	 * 
	 * @return
	 */
	public Map<Integer, String> getMainDict() {
		return intToString;
	}

	/**
	 * Read/Deserialize a dictionary from the disk to the memory
	 */
	public void loadFromDisk(String path) throws ClassNotFoundException, IOException {
		ObjectSerializerGZIP deserializerHashDictionary = ObjectSerializerGZIP.getInstance();
		intToString = deserializerHashDictionary.deserializeHashDictionary(path);		
	}
	
	/**
	 * Write/Serialize a dictionary and store it to disk
	 */
	public void toDisk(String pathToSave) throws FileNotFoundException, IOException {
		ObjectSerializerGZIP deserializerHashDictionary = ObjectSerializerGZIP.getInstance();
		deserializerHashDictionary.serializeHashDictionary(intToString, pathToSave);		
	}

	@Override
	public void toPersistentStorage(DbController db) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDictionary(Map<Integer, String> dictionaryFromDb) {
		// TODO Auto-generated method stub
		
	}
}