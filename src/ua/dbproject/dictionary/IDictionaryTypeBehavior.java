package ua.dbproject.dictionary;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import ua.dbproject.db.DbController;

/**
 * Interface that describes the blueprints for a specific dictionary behavior
 * 
 * @author ktheodorakos
 *
 */
public interface IDictionaryTypeBehavior {

	/**
	 * Add a string within the dictionary, returns 
	 * 
	 * @param inString
	 * @return
	 */
	public int addString(String inString);
	
	/**
	 * Get the integer value of a string
	 * without storing it within the dictionary
	 */
	public int addStringOneWay(String inString);
	
	/**
	 * Clear all of the dictionary's elements
	 */
	public void flush();
	
	/**
	 * Get how many elements are stored within the dictionary
	 */
	public int getCount();
	
	/**
	 * Get the string value for a specific integer
	 */
	public String getString(int inIndex);
	
	/**
	 * Read/Deserialize a dictionary from the disk to the memory
	 */
	public void loadFromDisk(String path) throws ClassNotFoundException, IOException;
	
	/**
	 * Write/Serialize a dictionary and store it to disk
	 */
	public void toDisk(String pathToSave) throws FileNotFoundException, IOException;

	public int getInt(String strIn);

	public void toPersistentStorage(DbController db) throws SQLException;

	public void setDictionary(Map<Integer, String> dictionaryFromDb);

	public Map<Integer, String> getMainDict();

}
