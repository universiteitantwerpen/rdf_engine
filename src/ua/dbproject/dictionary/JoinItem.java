package ua.dbproject.dictionary;

/**
 * Class used in SparqlQuery generation
 * 
 * @author ktheodorakos
 *
 */
public final class JoinItem {
	
	private String table = "";
	private String type = "";
	
	public JoinItem(String table, String type) {
		this.table = table;
		this.type = type;
	}
	
	public String getTable() {
		return this.table;
	}
	
	public String getType() {
		return this.type;
	}

}
