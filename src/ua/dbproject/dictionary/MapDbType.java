package ua.dbproject.dictionary;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Map;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;
import ua.dbproject.db.DbController;
import ua.dbproject.optimizer.StopWatch;
import ua.dbproject.parsers.Messenger;

/**
 * Static class that contains the algorithm for a HashMap type
 * of dictionary. Part of Strategy pattern
 * 
 * @author madks_000
 *
 */
public class MapDbType implements IDictionaryTypeBehavior, Serializable{

	private static final long serialVersionUID = 1L;
	private static DB db = null;
	private static HTreeMap<Integer, String> intToString = null;
	
	public MapDbType() {
		refreshDb();
		
		intToString = db.getHashMap("dict");
	}
	
	private void refreshDb() {
		db = DBMaker.newMemoryDB()
				.closeOnJvmShutdown()
				.transactionDisable()		// w/out: 151s, w: 137		
	            .asyncWriteFlushDelay(100) // w/out: 151s, w: 137
				.make();
	}
	
	public void toPersistentStorage(DbController dbController) throws SQLException {

		StopWatch dictToDb = new StopWatch();
		Messenger.show("Mem to disk start...");
		dictToDb.start();
		Messenger.show("Count: " + intToString.size());
		
		dbController.dictionaryToDb(intToString);
		intToString.clear();
		
		dictToDb.stop();
		Messenger.show("Mem to disk ended in " + dictToDb.elapsedSeconds() + " seconds.");
	}

	/**
	 * Adds a string in the dictionary.
	 * 
	 * @return Returns Java's hashCode for this string value
	 */
	public int addString(String inString) {
		int intToReturn = -1;
		
		intToReturn = inString.hashCode();
		if (!intToString.containsKey(intToReturn)) {
			intToString.put(intToReturn, inString);
		}
		return intToReturn;
	}
	
	/**
	 * Clear all of the dictionary's elements
	 */
	public void flush() {
		intToString.clear();
	}
		
	/**
	 * Get how many elements are stored within the dictionary
	 */
	public int getCount() {
		return intToString.size();
	}
	
	/**
	 * Get the string value for a specific integer (hashCode)
	 */
	public String getString(int inIndex) {
		String strToReturn = "";
		if(intToString.containsKey(inIndex))
			strToReturn = intToString.get(inIndex);
		
		return strToReturn;
	}

	/**
	 * Get the hashcode of a specific string, without storing
	 * it in the dictionary
	 * 
	 * @param strIn
	 * @return
	 */
	public int getInt(String strIn) {
		int intToReturn = -1;		
		intToReturn = strIn.hashCode();
		return intToReturn;
	}
	
	/**
	 * Get the instance of the HashMap dictionary that stores
	 * all the dictionary values
	 * 
	 * @return
	 */
	public Map<Integer, String> getMainDict() {
		return intToString;
	}
	
	/**
	 * Read/De-serialize a dictionary from the disk to the memory
	 */
	public void loadFromDisk(String path) throws ClassNotFoundException, IOException {		
		refreshDb();			
		intToString = db.getHashMap("dict");
	}
	
	/**
	 * Write/Serialize a dictionary and store it to disk
	 */
	public void toDisk(String pathToSave) throws FileNotFoundException, IOException {
		db.commit();
	}


	@Override
	public int addStringOneWay(String inString) {
		return -1;
	}

	@Override
	public void setDictionary(Map<Integer, String> dictionaryFromDb) {
	}
}