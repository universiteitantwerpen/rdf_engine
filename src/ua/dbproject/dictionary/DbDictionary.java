package ua.dbproject.dictionary;

import java.sql.SQLException;
import java.util.Map;

import ua.dbproject.db.DbController;

/**
 * HashDictionary uses Singleton instance using the HashMapType behavior (Stategy Pattern) 
 * @version 0.1
 * @author madks_000
 */
public final class DbDictionary extends GenericDictionary {
	
	private static DbDictionary uniqueInstance = null;
	
	/**
	 * Private constructor
	 */
	private DbDictionary() {
		dictionaryTypeBehavior = new DbType();
	}
	
	/**
	 * Acquire the unique instance of the HashDictionary using lazy instantiation
	 * 
	 * @return the unique instance
	 */
	public static DbDictionary getInstance() {
		if(uniqueInstance == null)
			uniqueInstance = new DbDictionary();
		return uniqueInstance;
	}
	
	public void toPersistentStorage(DbController db) throws SQLException {
		dictionaryTypeBehavior.toPersistentStorage(db);		
	}

	public void close() {
		// TODO Auto-generated method stub
		
	}

	public Map<Integer, String> getMainDict() {
		return dictionaryTypeBehavior.getMainDict();
	}
	
	public void setDictionary(Map<Integer, String> dictionaryFromDb) {
		dictionaryTypeBehavior.setDictionary(dictionaryFromDb);
	}	
}
