package ua.dbproject.dictionary;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import ua.dbproject.db.DbController;
import ua.dbproject.serializer.ObjectSerializerGZIP;

/**
 * Dictionary implementation that uses Array List
 * 
 * @author madks_000
 *
 */
public class ArrayListType implements IDictionaryTypeBehavior {	

	/**
	 * The ArrayList that stores all the strings
	 */
	private static ArrayList<String> stringList = new ArrayList<String>();
	
	/**
	 * Get the integer hashCode value of a string
	 * without storing it within the dictionary
	 */
	public int addStringOneWay(String inString) {
		return inString.hashCode();
	}
	
	/**
	 * Clear all of the dictionary's elements
	 */
	public void flush() {
		stringList.clear();
	}
	
	/**
	 * Get how many elements are stored within the dictionary
	 */
	public int getCount() {
		return stringList.size();
	}
	
	/**
	 * Get the string value for a specific integer (hashCode)
	 */
	public String getString(int inIndex) {
		return stringList.get(inIndex);
	}
	
	/**
	 * Write/Serialize a dictionary and store it to disk
	 */
	public static void ToDisk(String pathToSave) throws FileNotFoundException, IOException {
		ObjectSerializerGZIP arrayDictionarySerializer = ObjectSerializerGZIP.getInstance(); 
		arrayDictionarySerializer.serialize(stringList, pathToSave);		
	}	

	@Override
	public void loadFromDisk(String path) throws ClassNotFoundException,
			IOException {
		// TODO Auto-generated method stub
		
	}

	public int addString(String inString) {
		int returnInt = inString.hashCode();
		
		//TODO: if stringList doen'st have 'intString', add it and get the index
//		if(!intToString.containsKey(inString)) {
//			intToString.put(returnInt, inString);
//		}
		return returnInt;		
	}

	@Override
	public void toDisk(String pathToSave) throws FileNotFoundException,
			IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getInt(String strIn) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void toPersistentStorage(DbController db) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDictionary(Map<Integer, String> dictionaryFromDb) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Map<Integer, String> getMainDict() {
		// TODO Auto-generated method stub
		return null;
	}
}
