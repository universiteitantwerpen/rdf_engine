package ua.dbproject.dictionary;

/**
 * HashDictionary uses Singleton instance using the HashMapType behavior (Stategy Pattern) 
 * @version 0.1
 * @author madks_000
 */
public final class HashDictionary extends GenericDictionary {
	
	private static HashDictionary uniqueInstance = null;
	
	/**
	 * Private constructor
	 */
	private HashDictionary() {
		dictionaryTypeBehavior = new HashMapType();
	}
	
	/**
	 * Acquire the unique instance of the HashDictionary using lazy instantiation
	 * 
	 * @return the unique instance
	 */
	public static HashDictionary getInstance() {
		if(uniqueInstance == null)
			uniqueInstance = new HashDictionary();
		return uniqueInstance;
	}	
}
