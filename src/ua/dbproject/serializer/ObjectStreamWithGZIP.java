package ua.dbproject.serializer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import ua.dbproject.indexer.Triple;

/**
 * Concrete behavior of Object stream with Gzip appliance (Strategy pattern)
 */
public class ObjectStreamWithGZIP implements ISerializeBehavior {
	
	/**
	 * Serialize a generic serializable object to disk 
	 */
	public void serialize(Object toSerialize, String filename) throws IOException {
		FileOutputStream fileOut = new FileOutputStream(filename);
		ObjectOutputStream out = new ObjectOutputStream(new GZIPOutputStream(fileOut));
		out.writeObject(toSerialize);
		out.close();
		fileOut.close();
	}
	
	/**
	 * DeSerialize a HashSet<Triple<Integer, Integer, Integer> from disk to memory 
	 */
	@SuppressWarnings("unchecked")
	public HashSet<Triple<Integer, Integer, Integer>> deSerializeTripleHashSet(String fileName) throws ClassNotFoundException, IOException {
        // Declare the hashtable reference.
		HashSet<Triple<Integer, Integer, Integer>> returnObject = null;

    	FileInputStream fileIn = new FileInputStream(fileName);
    	ObjectInputStream inObject = new ObjectInputStream(new GZIPInputStream(fileIn));
    	returnObject = (HashSet<Triple<Integer, Integer, Integer>>)inObject.readObject();
    	inObject.close();
         
        return returnObject;
	}
	
	/**
	 * DeSerialize a HashMap<Integer, String> from disk to memory
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer, String> deserializeHashDictionary(String fileName) throws IOException, ClassNotFoundException {
        // Declare the hashtable reference.
		 Map<Integer, String> returnObject = new HashMap<Integer, String>();
		 FileInputStream fileIn = null;
		 
		 try {
	    	fileIn = new FileInputStream(fileName);
		 } catch (FileNotFoundException ex) {
			 fileIn = null;
		 }
		 
		 if (fileIn != null) {
	    	ObjectInputStream inObject = new ObjectInputStream(new GZIPInputStream(fileIn));
	    	returnObject = (Map<Integer, String>)inObject.readObject();
	    	inObject.close();
		 }
    	
        return returnObject;
	}

	/** 
	 * DeSerialize a HashSet<String> from disk to memory
	 */
	@SuppressWarnings("unchecked")
	public HashSet<Integer> deserializeIntHashSet(String fileName)  throws IOException, ClassNotFoundException {
        // Declare the hashtable reference.
        HashSet<Integer> returnObject = null;

    	FileInputStream fileIn = new FileInputStream(fileName);
    	ObjectInputStream inObject = new ObjectInputStream(new GZIPInputStream(fileIn));
    	returnObject = (HashSet<Integer>)inObject.readObject();
    	inObject.close();
    	
        return returnObject;
	}
	
	/**
	 * Serialize a HashSet<AbstractMap.SimpleEntry<Integer, Integer>> to disk
	 */
	@SuppressWarnings("unchecked")
	public HashSet<AbstractMap.SimpleEntry<Integer, Integer>> deserializeHashSetAbstractMap(String fileName) throws IOException, ClassNotFoundException {
        // Declare the hashtable reference.
		HashSet<AbstractMap.SimpleEntry<Integer, Integer>> returnObject = null;

    	FileInputStream fileIn = new FileInputStream(fileName);
    	ObjectInputStream inObject = new ObjectInputStream(new GZIPInputStream(fileIn));
    	returnObject = (HashSet<AbstractMap.SimpleEntry<Integer, Integer>>)inObject.readObject();
    	inObject.close();
    	
        return returnObject;
	}
}
