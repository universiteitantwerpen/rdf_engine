package ua.dbproject.serializer;

/**
 * Serialize different dictionary objects to binary files
 * @version 0.1
 * @author madks_000
 */
public final class ObjectSerializer extends GenericSerializer {

	private static final long serialVersionUID = 1L;
	private static ObjectSerializer uniqueInstance = null;
	
	/**
	 * Public constructor
	 */
	public ObjectSerializer() {
		serializeBehavior = new ObjectStream();
	}
	
	/**
	 * Acquire the unique instance of the ObjectSerializer using lazy instantiation
	 * 
	 * @return the unique instance
	 */
	public static ObjectSerializer getInstance() {
		if (uniqueInstance == null)
			uniqueInstance = new ObjectSerializer();

		return uniqueInstance;
	}
	
}
