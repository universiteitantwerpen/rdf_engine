package ua.dbproject.serializer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.AbstractMap;
import java.util.HashSet;
import java.util.Map;

import ua.dbproject.indexer.Triple;

/**
 * Abstract Serialize class that contains the basic serialization functions
 *
 */
public abstract class GenericSerializer implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ISerializeBehavior serializeBehavior;
	
	/**
	 * Choose between simple object stream or object stream with Gzip compression (Strategy pattern)
	 * @param sb The ISerializeBehavior concrete strategy can be set here
	 */
	public void setSerializeBehavior(ISerializeBehavior sb) {
		serializeBehavior = sb;
	}
	
	/**
	 * Serialize a generic object
	 * 
	 * @param objectToSerialize
	 * @param fileName
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void serialize(Object objectToSerialize, String fileName) throws FileNotFoundException, IOException {
		serializeBehavior.serialize(objectToSerialize, fileName);
	}
	
	/**
	 * Serialize a HashSet<Triple<Integer, Integer, Integer> to disk 
	 * 
	 * @throws IOException 
	 * @throws FileNotFoundException
	 */
	public void serializeHashDictionary(Map<Integer, String> toSerialize, String filename) throws FileNotFoundException, IOException {
		serializeBehavior.serialize(toSerialize, filename);
	}
	
	/** 
	 * Serialize a HashSet<Triple<Integer, Integer, Integer> to disk 
	 * 
	 * @throws IOException 
	 * @throws FileNotFoundException
	 */
	public void serializeTripleHashSet(HashSet<Triple<Integer, Integer, Integer>> toSerialize, String filename) throws FileNotFoundException, IOException {
		serializeBehavior.serialize(toSerialize, filename);
	}
	
	/** 
	 * Serialize a HashSet<AbstractMap.SimpleEntry<Integer, Integer>> to disk 
	 * 
	 * @throws IOException 
	 * @throws FileNotFoundException
	 */
	public void serializeHashSetAbstractMap(HashSet<AbstractMap.SimpleEntry<Integer, Integer>> toSerialize, String filename) throws FileNotFoundException, IOException {
		serializeBehavior.serialize(toSerialize, filename);
	}
	
	/** 
	 * Serialize a HashSet serializable object to disk 
	 * 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public void serializeIntHashSet(HashSet<Integer> toSerialize, String filename) throws FileNotFoundException, IOException {
		serializeBehavior.serialize(toSerialize, filename);
	}

	/** 
	 * DeSerialize a HashSet<Triple<Integer, Integer, Integer> from disk to memory
	 */
	public HashSet<Triple<Integer, Integer, Integer>> deSerializeTripleHashSet(String fileName) throws IOException, ClassNotFoundException {
        return serializeBehavior.deSerializeTripleHashSet(fileName);
	}
	
	/** 
	 * DeSerialize a HashMap<Integer, String> from disk to memory
	 */
	public Map<Integer, String> deserializeHashDictionary(String fileName) throws IOException, ClassNotFoundException {
        return serializeBehavior.deserializeHashDictionary(fileName);
	}

	/** 
	 * DeSerialize a HashSet<String> from disk to memory
	 */
	public HashSet<Integer> deserializeIntHashSet(String fileName)  throws IOException, ClassNotFoundException {
        return serializeBehavior.deserializeIntHashSet(fileName);
	}
	
	/** Serialize a HashSet<AbstractMap.SimpleEntry<Integer, Integer>> to disk 
	 * @throws IOException 
	 * @throws ClassNotFoundException
	 */
	public HashSet<AbstractMap.SimpleEntry<Integer, Integer>> deserializeHashSetAbstractMap(String fileName) throws IOException, ClassNotFoundException {
        return serializeBehavior.deserializeHashSetAbstractMap(fileName);
	}
	
} 