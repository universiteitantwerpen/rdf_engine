package ua.dbproject.serializer;

import java.io.FileInputStream;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.AbstractMap;
import java.util.HashSet;
import java.util.Map;
import ua.dbproject.indexer.Triple;

/**
 * Concrete behavior of Object stream (Strategy pattern)
 */
public class ObjectStream implements ISerializeBehavior {
	
	/** 
	 * Serialize a generic serializable object to disk 
	 * @throws IOException
	 */
	public void serialize(Object toSerialize, String filename) throws IOException {
		FileOutputStream fileOut = new FileOutputStream(filename);
		ObjectOutputStream out = new ObjectOutputStream(fileOut);
		out.writeObject(toSerialize);
		out.close();
		fileOut.close();
	}
	
	/** 
	 * DeSerialize a HashSet<Triple<Integer, Integer, Integer> from disk to memory 
	 * @throws IOException 
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public HashSet<Triple<Integer, Integer, Integer>> deSerializeTripleHashSet(String fileName) throws ClassNotFoundException, IOException {
        // Declare the hashtable reference.
		HashSet<Triple<Integer, Integer, Integer>> returnObject = null;

    	FileInputStream fileIn = new FileInputStream(fileName);
    	ObjectInputStream inObject = new ObjectInputStream(fileIn);
    	returnObject = (HashSet<Triple<Integer, Integer, Integer>>)inObject.readObject();
    	inObject.close();
         
        return returnObject;
	}
	
	/** 
	 * DeSerialize a HashMap<Integer, String> from disk to memory
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer, String> deserializeHashDictionary(String fileName) throws IOException, ClassNotFoundException {
        // Declare the hashtable reference.
		 Map<Integer, String> returnObject = null;

    	FileInputStream fileIn = new FileInputStream(fileName);
    	ObjectInputStream inObject = new ObjectInputStream(fileIn);
    	returnObject = (Map<Integer, String>)inObject.readObject();
    	inObject.close();
    	
        return returnObject;
	}

	/** 
	 * DeSerialize a HashSet<String> from disk to memory */
	@SuppressWarnings("unchecked")
	public HashSet<Integer> deserializeIntHashSet(String fileName)  throws IOException, ClassNotFoundException {
        // Declare the hashtable reference.
        HashSet<Integer> returnObject = null;

    	FileInputStream fileIn = new FileInputStream(fileName);
    	ObjectInputStream inObject = new ObjectInputStream(fileIn);
    	returnObject = (HashSet<Integer>)inObject.readObject();
    	inObject.close();
    	
        return returnObject;
	}
	
	/** 
	 * Serialize a HashSet<AbstractMap.SimpleEntry<Integer, Integer>> to disk 
	 * @throws IOException 
	 * @throws ClassNotFoundException */
	@SuppressWarnings("unchecked")
	public HashSet<AbstractMap.SimpleEntry<Integer, Integer>> deserializeHashSetAbstractMap(String fileName) throws IOException, ClassNotFoundException {
        // Declare the hashtable reference.
		HashSet<AbstractMap.SimpleEntry<Integer, Integer>> returnObject = null;

    	FileInputStream fileIn = new FileInputStream(fileName);
    	ObjectInputStream inObject = new ObjectInputStream(fileIn);
    	returnObject = (HashSet<AbstractMap.SimpleEntry<Integer, Integer>>)inObject.readObject();
    	inObject.close();
    	
        return returnObject;
	}
}
