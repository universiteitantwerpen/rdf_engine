package ua.dbproject.serializer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.HashSet;
import java.util.Map;

import ua.dbproject.indexer.Triple;

/**
 * Interface that helps in defining a custom serializing behavior, ie GZIP, object stream etc
 *
 */
public interface ISerializeBehavior {
	
	/**
	 * Serialize a generic serializable object to disk 
	 * 
	 * @param toSerialize Any object
	 * @param filename the filePath
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void serialize(Object toSerialize, String filename) throws FileNotFoundException, IOException;

	/**
	 * DeSerialize a HashSet<Triple<Integer, Integer, Integer> from disk to memory
	 * 
	 * @param fileName
	 * @return HashSet<Triple<Integer, Integer, Integer>> to memory
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public HashSet<Triple<Integer, Integer, Integer>> deSerializeTripleHashSet(String fileName) throws ClassNotFoundException, IOException;
	
	/**
	 * DeSerialize a HashMap<Integer, String> from disk to memory 
	 * 
	 * @param fileName
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public Map<Integer, String> deserializeHashDictionary(String fileName) throws IOException, ClassNotFoundException;
	
	/**
	 * DeSerialize a HashSet<String> from disk to memory 
	 * 
	 * @param fileName
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public HashSet<Integer> deserializeIntHashSet(String fileName) throws IOException, ClassNotFoundException;
	
	/**
	 * Serialize a HashSet<AbstractMap.SimpleEntry<Integer, Integer>> to disk 
	 * 
	 * @param fileName
	 * @return HashSet<AbstractMap.SimpleEntry<Integer, Integer>>
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public HashSet<AbstractMap.SimpleEntry<Integer, Integer>> deserializeHashSetAbstractMap(String fileName) throws IOException, ClassNotFoundException;
}