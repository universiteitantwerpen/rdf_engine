package ua.dbproject.serializer;


/**
 * Serialize different dictionary objects to binary files using GZIP compression
 */
public final class ObjectSerializerGZIP extends GenericSerializer {

	private static final long serialVersionUID = 1L;
	private static ObjectSerializerGZIP uniqueInstance = null;
	
	public ObjectSerializerGZIP() {
		serializeBehavior = new ObjectStreamWithGZIP();
	}
	
	/**
	 * Get the unique instance using lazy initialization 
	 *
	 * @return
	 */
	public static ObjectSerializerGZIP getInstance() {
		if (uniqueInstance == null)
			uniqueInstance = new ObjectSerializerGZIP();

		return uniqueInstance;
	}
}
