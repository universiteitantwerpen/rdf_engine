package ua.dbproject.optimizer;

import java.util.concurrent.TimeUnit;

/**
 * Class that helps in counting the execution timeout of operations
 * 
 * @author ktheodorakos
 *
 */
public class StopWatch {
	
	private long startTime = 0;
	private long stopTime = 0;	

	public StopWatch() {
		reset();
	}
	
	/**
	 * Reset the start and stop time
	 */
	public void reset() {
		startTime = 0;
		stopTime = 0;				
	}

	/**
	 * Start the timer
	 */
	public void start() {
		startTime = System.nanoTime();		
	}
	
	/**
	 * Stop the timer
	 */
	public void stop() {
		stopTime = System.nanoTime();		
	}
	
	/**
	 * Get the elapsed time in milliseconds
	 * 
	 * @return
	 */
	public long elapsed() {
		return TimeUnit.NANOSECONDS.toMillis(stopTime - startTime);
	}
	
	/**
	 * Get the elapsed time in seconds
	 * 
	 * @return
	 */
	public long elapsedSeconds() {
		return TimeUnit.NANOSECONDS.toSeconds(stopTime - startTime);
	}
	
	/**
	 * Get the elapsed interval time in seconds
	 * 
	 * @return
	 */
	public long elapsedIntervalMilliSeconds() {
		
		return TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime);
	}
}
