# RDF Database Engine#

### Characteristics
- Main project for the course "Project Databases".
- Uses internal indexers, parsers and serializers to map *SPARQL* queries to an SQL Database.
- Can use *row-based* (PostgreSQL) or *column-based* (MonetDB) DBMS internally.
- Successfully parsed *15.4 GB of data triplets* and able to query the data using SPARQL.
- It was deployed on a local computer (windows 7), on a Debian Linux server and on an *Amazon Elastic Compute AMI* that was expanded from t2.micro to m3.2xlarge for test purposes.

###Documentation
- Features and Results **Presentation**: https://onedrive.live.com/redir?resid=F3C315EB7F683B03!17471&authkey=!ACBLmwZ1zBFCs3c&ithint=file%2cpptx
- Full **Documentation**: https://onedrive.live.com/redir?resid=F3C315EB7F683B03!17473&authkey=!ADLqfjctwt4DIqs&ithint=file%2cpdf

###References
1. *MonetDB5 Reference Manual* - http://www.gevgb.com/monetdb/m5manual.pdf
2. *N-Triples Format* - http://en.wikipedia.org/wiki/N-Triples
3. *N-Triples Format 2* - http://www.w3.org/2001/sw/RDFCore/ntriples/
4. *XML Schema Data types in RDF* - http://www.w3.org/TR/swbp-xschdatatypes/
5. *Billion Triples Challenge 2012* - http://km.aifb.kit.edu/projects/btc-2012/
6. *Apache Jena Getting Started* - http://jena.apache.org/getting_started/index.html
7. *MonetDB introduction* - http://www.earthobservatory.eu/MonetDB
8. *Online SPARQL Query demo* - http://www.sparql.org/query.html
9. *Allegrograph RDF Engine* - http://franz.com/agraph/allegrograph/
10. *DBPedia* - http://dbpedia.org/About
11. *Populating PostgreSQL Databases* - http://www.postgresql.org/docs/8.3/static/populate.html
12. *Essential PostgreSQL Dzone Refcardz* - http://refcardz.dzone.com/refcardz/essential-postgresql
13. *MonetDB read-only database set up* - https://www.monetdb.org/Documentation/readonly-database
14. *MonetDB Optimizer Pipelines* - https://www.monetdb.org/Documentation/Cookbooks/SQLrecipes/OptimizerPipelines
15. *Database normalization* - http://en.wikipedia.org/wiki/Database_normalization#Normal_forms
16. *Regular Expressions* - http://refcardz.dzone.com/refcardz/regular-expressions
17. *MapDB Getting Started* - http://www.mapdb.org/02-getting-started.html